name := "cumulative"

version := "0.1"



scalaVersion := "2.13.1"

//mainClass := Some("Main")


enablePlugins(PackPlugin)

resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot/"

libraryDependencies += "oscar" %% "oscar-cp" % "4.1.0-SNAPSHOT" withSources()


libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"




scalacOptions in Compile ++= Seq("-feature", "-opt-warnings", "-opt:l:method", "-opt:l:inline", "-opt-inline-from:scala.**,oscar.**,MaxSubSubmatrix.**")