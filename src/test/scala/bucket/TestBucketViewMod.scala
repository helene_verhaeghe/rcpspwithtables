package bucket

import org.scalatest.FunSuite
import oscar.cp._
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPIntVar

class TestBucketViewMod extends FunSuite {

  test("Simple test") {
    val nPart = 4
    val n = 18
    val min = 2
    val max = min + n - 1
    val rightbound = Array((0, 0, 4), (1, 5, 9), (2, 10, 13), (3, 14, 17)).map(t => (t._1,min+t._2,min+t._3))
    // 0 -> 4, 5->9, 10->13, 14->18
    val solver = CPSolver()
    val x = CPIntVar(min to max)(solver)
    val b = CPIntVar(0 until nPart)(solver)

    val cst = new BucketViewMod(x, b, min, max, nPart)
    solver.add(cst)
    for (i <- min until max)
      assert(x.hasValue(i))
    for (i <- 0 until nPart)
      assert(b.hasValue(i))

    val v1 = 1
    solver.add(b !== v1)
    for ((id, lb, ub) <- rightbound)
      if (id == v1) {
        assert(!b.hasValue(id))
        for (k <- lb to ub)
          assert(!x.hasValue(k))
      } else {
        assert(b.hasValue(id))
        for (k <- lb to ub)
          assert(x.hasValue(k))
      }

    val w1 = min+1
    solver.add(x !== w1)
    for ((id, lb, ub) <- rightbound)
      if (id == v1) {
        assert(!b.hasValue(id))
        for (k <- lb to ub)
          assert(!x.hasValue(k))
      } else {
        assert(b.hasValue(id))
        for (k <- lb to ub) {
          if (k == w1)
            assert(!x.hasValue(k))
          else
            assert(x.hasValue(k))
        }
      }

    val v2 = 3
    solver.add(b !== v2)
    for ((id, lb, ub) <- rightbound)
      if (id == v1 || id == v2) {
        assert(!b.hasValue(id))
        for (k <- lb to ub)
          assert(!x.hasValue(k))
      } else {
        assert(b.hasValue(id))
        for (k <- lb to ub) {
          if (k == w1)
            assert(!x.hasValue(k))
          else
            assert(x.hasValue(k))
        }
      }

    val w2 = min+0
    solver.add(x !== w2)
    val w3 = min+2
    solver.add(x !== w3)
    val w4 = min+3
    solver.add(x !== w4)
    val w5 = min+4
    solver.add(x !== w5)
    for ((id, lb, ub) <- rightbound)
      if (id == v1 || id == v2 || id == 0) {
        assert(!b.hasValue(id))
        for (k <- lb to ub)
          assert(!x.hasValue(k))
      } else {
        assert(b.hasValue(id))
        for (k <- lb to ub) {
          if (k == w1)
            assert(!x.hasValue(k))
          else
            assert(x.hasValue(k))
        }
      }
  }
}
