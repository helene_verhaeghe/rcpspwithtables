package rcpsp

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Read the file. The format is the following:
 * nTask nRes
 * capa_1 capa_2 ... capa_nRes
 * dur_1 dem_1_1 dem_1_2 ... dem_1_nRes k1 succ_1_1 succ_1_2 ... succ_1_k1
 * dur_2 dem_2_1 dem_2_2 ... dem_2_nRes k2 succ_2_1 succ_2_2 ... succ_2_k2
 * ...
 * dur_nTask dem_nTask_1 dem_nTask_2 ... dem_nTask_nRes knTask succ_nTask_1 succ_nTask_2 ... succ_nTask_knTask
 */
class RCPSPParser {

  def parser(fileName:String, format:String = "BL") = {
    format match {
      case "BL" => parserBL(fileName)
      case "J" => parserJ(fileName)
      case _ => throw new Exception("wrong format (BL or J)")
    }
  }

  def parserBL(fileName: String): RCPSPInstance = {
    val lines = Source.fromFile(fileName).getLines
    val line1 = lines.next().trim.split("[ ,\t]+")
    val nTask = line1(0).toInt
    val nRes = line1(1).toInt
    val line2 = lines.next().trim.split("[ ,\t]+")
    val Capacity: Array[Int] = Array.tabulate(nRes)(i => line2(i).toInt)
    val duration: Array[Int] = new Array[Int](nTask)
    val demande: Array[Array[Int]] = Array.ofDim[Int](nTask, nRes)
    val precedence: ArrayBuffer[(Int, Int)] = new ArrayBuffer[(Int, Int)]()
    val matrix: Array[Array[Int]] = Array.ofDim[Int](nTask, nTask)
    var i = 0
    while (lines.hasNext) {
      val line = lines.next()
      val data = line.trim.split("[ ,\t]+")
      duration(i) = data(0).toInt
      for (k <- 0 until nRes) {
        demande(i)(k) = data(1 + k).toInt
      }
      val nPred = data(nRes + 1).toInt
      for (j <- 0 until nPred) {
        val k = data(nRes + 2 + j).toInt - 1
        matrix(i)(k) = 1
      }
      i += 1
    }
    for (k <- 0 until nTask; i <- 0 until nTask; j <- 0 until nTask) {
      if (matrix(i)(k) == 1 && matrix(k)(j) == 1 && matrix(i)(j) != 1)
        matrix(i)(j) = 1
    }
    for (i <- 0 until nTask; j <- i + 1 until nTask) {
      if (matrix(i)(j) == 1) precedence += ((i, j))
    }

    RCPSPInstance(nTask, nRes, Capacity, duration, demande, precedence.toArray)
  }

  def parserJ(fileName: String) = {
    val lines = Source.fromFile(fileName).getLines
    for (i <- 0 until 5) lines.next()
    val nTask = lines.next().split("\\D+").filter(_.nonEmpty)(0).toInt
    for (i <- 0 until 2) lines.next()
    val nRes = lines.next().split("\\D+").filter(_.nonEmpty)(0).toInt
    for (i <- 0 until 9) lines.next()
    val precedence: ArrayBuffer[(Int, Int)] = new ArrayBuffer[(Int, Int)]()
    for (i <- 0 until nTask){
      val line =lines.next().split("\\D+").filter(_.nonEmpty).map(_.toInt)
      for (j <- 0 until line(2))
        precedence += ((i, line(3+j)-1))
    }
    for (i <- 0 until 4) lines.next()
    val duration: Array[Int] = new Array[Int](nTask)
    val demande: Array[Array[Int]] = Array.ofDim[Int](nTask, nRes)

    for (i <- 0 until nTask){
      val line =lines.next().split("\\D+").filter(_.nonEmpty).map(_.toInt)
      duration(i) = line(2)
      for (j <- 0 until nRes)
        demande(i)(j) = line(3+j)
    }
    for (i <- 0 until 3) lines.next()
    val Capacity = lines.next().split("\\D+").filter(_.nonEmpty).map(_.toInt)
    RCPSPInstance(nTask, nRes, Capacity, duration, demande, precedence.toArray)
  }
}