//package rcpsp
//
//import oscar.algo.Inconsistency
//import oscar.cp._
//
//class TableGeneration(newBounds: Array[(Int,Int)],
//                      durationD: Array[Int],
//                      heightD: Array[Array[Int]],
//                      Capacity: Array[Int],
//                      i: Int,
//                      precedence: Array[(Int, Int)],
//                      nPart: Int) extends CPModel{
//  private val nTask = newBounds.length
//  private val nRes = Capacity.length
//  def solveLeft(): Array[Int] = {
//    val starts: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(newBounds(i)._1 to newBounds(i)._2))
//    val duration: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(durationD(i)))
//    val ends: Array[CPIntVar] = Array.tabulate(nTask)(i => starts(i) + duration(i))
//
//    // caonstraints
//    for((s,e) <- precedence)
//      add(ends(s) <= starts(e))
//    for(r <- 0 until nRes){
//      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightD(i)(r)))
//      val resource: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(r))
//      if(!tryPost(maxCumulativeResource(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r)))
//        return Array.fill(nTask)(-1)
//    }
//    // on solution
//    var solution: Array[Int] = Array.tabulate(nTask)(_ => -1)
//    onSolution{
//      solution = Array.tabulate(nTask)(i => starts(i).value)
//    }
//    // search
//    search{
//      conflictOrderingSearch(starts, i => starts(i).min, i => starts(i).min)
//    }
//    start(1)
//
//    solution
//  }
//
//
//
//  private def tryPost(c: Constraint): Boolean = {
//    try {
//      solver.post(c)
//    } catch {
//      case _: Inconsistency => return false
//    }
//    true
//  }
//
//}
