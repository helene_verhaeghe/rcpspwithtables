//package rcpsp
//
//import oscar.cp._
//import oscar.cp.core.CPPropagStrength
//import oscar.cp.scheduling.constraints.MaxCumulative
//import bucket.{BucketView, Partitions}
//
//import scala.collection.mutable.ArrayBuffer
//
//object newTestFor extends CPModel with App{
////  val nTask = 5
////  val nRes = 1
////  val nPart = 4
////  val precedence: Array[(Int, Int)] = Array((0,1), (0,2),(0,3),(0,4),(1,4),(2,4),(3,4))
////  val Capacity: Array[Int] = Array(5)
////  val heightData: Array[Array[Int]] = Array(Array(3),Array(2),Array(4),Array(3),Array(3))
////  val durationData: Array[Int] = Array(7,9,5,7,6)
////  val horizon = durationData.sum
////  println(horizon)
////
////  val data = new RCPSPInstance(nTask, nRes, Capacity, durationData, heightData, precedence)
//  val data = new RCPSPParser().parser("./data/BL/bl25_1.rcp")
//  val nTask = data.nTask
//  val nRes = data.nRes
//  val nPart = 4
//  val precedence = data.precedence
//  val Capacity = data.resCapacity
//  val heightData = data.taskUsage
//  val durationData = data.taskDuration
//  val horizon = durationData.sum
//  println(horizon)
//
//  val withTable = true
//  val firstsol = false
//
//  private val starts: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
//  val fakestart = CPIntVar(0 to horizon)
//  private val duration: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
//  private val ends: Array[CPIntVar] = Array.tabulate(nTask)(i => starts(i) + duration(i))
//
//  val remainingTime: Mut[Int] = new Mut[Int](10000)
//  // bucketing the starts variable
//  val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart)
//  if (!firstsol && withTable) {
//    val bucket: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(0 until nPart))
//    for (i <- starts.indices) {
//      val part = new Partitions(nPart, fakestart) //starts(i))
////      val part = new Partitions(nPart, starts(i))
//      println("i " + i)
//      bounds(i) = part.nContiguousBound()
//      println(bounds(i).mkString(", "))
//      add(BucketView(starts(i), bucket(i), bounds(i)))
//    }
//
//
//    val tableForRCPSP: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//    val tab = new TableGenerationForBucket(data: RCPSPInstance, nPart, tableForRCPSP, remainingTime)
//    println("finished " + tableForRCPSP.length + " tuples")
//    //  for(tuple <- tableForRCPSP) println(tuple.mkString(" - "))
//    println("----------------------------------------------------")
//    if (tableForRCPSP.nonEmpty) {
//      add(table(bucket, tableForRCPSP.toArray))
//    }
//  }
//
//  for ((s, e) <- precedence) {
//    add(ends(s) <= starts(e))
//  }
//
//  for (r <- 0 until nRes) {
//    val resource: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(r))
//    val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r)))
//    add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r), CPPropagStrength.Strong)
//  }
//
////  if (!firstsol){
////    for (i <- ends.indices)
////      add(ends(i) < 29)
////  }
//  val makespan = maximum(ends)
//
//  minimize(makespan)
//
//  // On solution
//  onSolution {
//    println(starts.mkString(" , ") + "\nmakespan: " + makespan.value + "\n-----------------------------")
//  }
//
//  search{
//    conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
////        conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
//  }
//
//  val stat = if (firstsol)
//    start(timeLimit = remainingTime.value,nSols = 1)
//  else
//    start(timeLimit = remainingTime.value)
//  println(stat)
//
//}
