//package rcpsp
//
//import oscar.cp.core.CPPropagStrength
//import oscar.cp.scheduling.constraints.MaxCumulative
//import oscar.cp._
//
//class SearchForFirstSolutionSpan(data: RCPSPInstance) extends CPModel with App{
//  private var span = -1
//  def firstSolutionMakeSpan(): Int ={
//    val numberOfTask = data.nTask
//    val numberOfResource = data.nRes
//    val Capacity = data.resCapacity
//    val durationData = data.taskDuration
//    val precedence = data.precedence
//    val demandeData = data.taskUsage
//    val horizon = durationData.sum
//
//    // variables
//    val starts: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(0 to horizon - durationData(i)))
//    val duration: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(durationData(i)))
//    val ends: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => starts(i) + duration(i))
//
//    // add constraints
//    // precedence constraint
//    for ((i,j) <- precedence) {
//      add(ends(i) <= starts(j))
//    }
//    // resource constraint
//    for (r <- 0 until numberOfResource) {
//      val resource: Array[CPIntVar] = Array.tabulate(numberOfTask)(_ => CPIntVar(r))
//      val height: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(demandeData(i)(r)))
//      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r), CPPropagStrength.Strong)
//    }
//    val makespan = maximum(ends)
//    // On solution
//    onSolution {
//      span = makespan.value
//      //println(starts.mkString(" , ") + "\nmakespan: " + makespan.value + "\n-----------------------------")
//    }
//    // search
//    search {
//      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
//    }
//    // statistics
//    start(1)
//    span
//  }
//
//}
