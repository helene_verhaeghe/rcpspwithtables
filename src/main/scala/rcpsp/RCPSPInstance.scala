package rcpsp

/**
 * Modelize an instance of the RCPSP problem
 */
case class RCPSPInstance(nTask: Int,
                         nRes: Int,
                         resCapacity: Array[Int],
                         taskDuration: Array[Int],
                         taskUsage: Array[Array[Int]],
                         precedence: Array[(Int, Int)]) {

  val maxHorizon: Int = taskDuration.sum

  override def toString: String = {
    var str = "nRes:\t" + nRes + "\n"
    str = str + "resources capacities: (resource id,resource capa) " + "\n"
    for (i <- 0 until resCapacity.size)
      str = str + "\t(" + i + "," + resCapacity(i) + ")\n"
    str = str + "nTask:\t" + nTask + "\n"
    str = str + "items: (item id, item duration,item usage by resources) " + "\n"
    for (i <- 0 until taskDuration.size)
      str = str + "\t(" + i + "," + taskDuration(i) + ", [" + taskUsage(i).mkString(",") + "] )\n"
    str = str + "prec:" + "\n"
    str = str + "\t"+precedence.map(x => x._1 + " -> " + x._2).mkString("\n\t") + "\n"

    str
  }

  def outputinfo() = {
    print(this.toString())
  }
}



case class Mut[A](var value: A) {}