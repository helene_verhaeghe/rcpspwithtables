//package rcpsp
//
//import bucket.{BucketView, Partitions}
//import oscar.algo.search.{DFSLinearizer, DFSReplayer, SearchStatistics}
//import oscar.cp._
//import oscar.cp.core.CPPropagStrength
//import oscar.cp.scheduling.constraints.MaxCumulative
//
//import scala.collection.mutable.ArrayBuffer
//
//
//class RCPSPWithTableFixed(data: RCPSPInstance,
//                          horizon: Int,
//                          timeLimitSearch: Int,
//                          timeLimitGenerateTable: Mut[Int] = new Mut[Int](0),
//                          typeOfTableToGenerate: Int,
//                          silent: Boolean = false) extends CPModel {
//
//  private var span: Int = -1
//  private var stat: SearchStatistics = null
//  private val sizeOfPartition: Mut[Int] = new Mut[Int](6)
//  private val numberOfTaskToPartition: Mut[Int] = new Mut[Int](10)
//
//  def solveTheProblem(): Unit = {
//    val numberOfTask = data.nTask
//    val numberOfResource = data.nRes
//    val Capacity = data.resCapacity
//    val durationData = data.taskDuration
//    val precedence = data.precedence
//    val demandeData = data.taskUsage
//
//    // variables
//    val starts: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(0 to horizon - durationData(i)))
//    val duration: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(durationData(i)))
//    val ends: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => starts(i) + duration(i))
//
//    // auxillary variables
//    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](numberOfTask, sizeOfPartition.value)
//    val bucket: Array[CPIntVar] = new Array[CPIntVar](numberOfTask)
//    val taskSortByTotalEneygy = starts.indices.sortBy(i => -durationData(i) * (0 until numberOfResource).map(r => demandeData(i)(r)).sum).toArray
//
//    val tableGenerate = generateTheTable(data, typeOfTableToGenerate, timeLimitGenerateTable)
//    for (i <- taskSortByTotalEneygy) {
//      if (taskSortByTotalEneygy.indexOf(i) < numberOfTaskToPartition.value) {
//        val partition = new Partitions(sizeOfPartition.value, starts(i))
//        bounds(i) = partition.nContiguousBound()
//        bucket(i) = CPIntVar(0 until sizeOfPartition.value)
//        add(BucketView(starts(i), bucket(i), bounds(i)))
//      } else {
//        val part = new Partitions(1, starts(i))
//        bounds(i) = part.nContiguousBound()
//        bucket(i) = CPIntVar(0)
//        add(BucketView(starts(i), bucket(i), bounds(i)))
//      }
//    }
//
//    // add other constraints
//    // precedence constraint
//    for ((i, j) <- precedence) {
//      add(ends(i) <= starts(j))
//    }
//
//    // resource constraint
//    for (r <- 0 until numberOfResource) {
//      val resource: Array[CPIntVar] = Array.tabulate(numberOfTask)(_ => CPIntVar(r))
//      val height: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(demandeData(i)(r)))
//      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r), CPPropagStrength.Strong)
//    }
//    // objective function
//    val makespan = maximum(ends)
//    minimize(makespan)
//
//    // On solution
//    onSolution {
//      span = makespan.value
//      if (silent)
//        println(starts.mkString(" , ") + "\nmakespan: " + makespan.value + "\n-----------------------------")
//    }
//
//    // search
//    search {
//      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
//    }
//
//    val timeFactor = 3
//    val linearizer: DFSLinearizer = new DFSLinearizer()
//
//    //if (!solver.silent) println("starting search without table")
//    val whitoutTable = startSubjectTo(searchListener = linearizer, timeLimit = timeLimitSearch) {
//      solver.objective.objs.head.relax()
//    }
//    println("whithout Table : " +whitoutTable.time + " | " + whitoutTable.nNodes + " | ")
//    //println(whitoutTable)
//    //if (!solver.silent) println("starting search with table")
//
//
//    val withTable = solver.replaySubjectTo(linearizer, starts, timeLimit = timeLimitSearch * timeFactor) {
//      solver.objective.objs.head.relax()
//      if (typeOfTableToGenerate == 1 && tableGenerate.nonEmpty) {
//        val partialBucket: Array[CPIntVar] = Array.tabulate(numberOfTaskToPartition.value)(i => bucket(taskSortByTotalEneygy(i)))
//        add(shortTable(partialBucket, tableGenerate.toArray))
//      } else {
//        if (tableGenerate.nonEmpty) {
//          add(table(bucket, tableGenerate.toArray))
//        }
//      }
//    }
//    println("with Table : " +withTable.time + " | " + withTable.nNodes + " | ")
//    //println(withTable)
//  }
//
//
//
//    def generateTheTable(data: RCPSPInstance, typeOfTableToGenerate: Int, timeLimitGenerateTable: Mut[Int]): ArrayBuffer[Array[Int]] = {
//      // data
//      val numberOfTask = data.nTask
//      val numberOfResource = data.nRes
//      val durationData = data.taskDuration
//      val demandeData = data.taskUsage
//      // auxillary variables
//      val taskSortByTotalEneygy = (0 until numberOfTask).indices.sortBy(i => -durationData(i) * (0 until numberOfResource).map(r => demandeData(i)(r)).sum).toArray
//      var finalTable: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//      var improveFinalTable: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//      if (numberOfTaskToPartition.value > 0 && sizeOfPartition.value > 0) {
//        val currentTableGenerate: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//        var build = false
//        typeOfTableToGenerate match {
//          case 0 => while (!build && sizeOfPartition.value < 2000) { // FVP variation of the size of the partition
//            currentTableGenerate.clear
//            val tab = new GenerateTable(data, horizon, currentTableGenerate, typeOfTableToGenerate,sizeOfPartition,
//              numberOfTaskToPartition, timeLimitGenerateTable)
//            tab.generateTheTable()
//            if (tab.isComplet()) {
//              finalTable.clear()
//              finalTable ++= currentTableGenerate
//              sizeOfPartition.value += 1
//            } else {
//              sizeOfPartition.value -= 1
//              build = true
//            }
//          }
//          case 1 => while (!build && numberOfTaskToPartition.value < numberOfTask) { // FSP variation of the number of tasks to partition
//            currentTableGenerate.clear
//            val tab = new GenerateTable(data, horizon, currentTableGenerate, typeOfTableToGenerate,sizeOfPartition,
//              numberOfTaskToPartition, timeLimitGenerateTable)
//            tab.generateTheTable()
//            if (tab.isComplet()) {
//              finalTable.clear()
//              finalTable ++= currentTableGenerate
//              numberOfTaskToPartition.value += 1
//            } else {
//              if (currentTableGenerate.nonEmpty && finalTable.nonEmpty && typeOfTableToGenerate == 1) {
//                val incompletTable = extractTable(currentTableGenerate, taskSortByTotalEneygy, numberOfTaskToPartition.value)
//                val completTable = extractTable(finalTable, taskSortByTotalEneygy, numberOfTaskToPartition.value - 1)
//                var found = false
//                var i = 0
//                while (!found && i < completTable.length) {
//                  if (isEqual(completTable(i), incompletTable.last))
//                    found = true
//                  i += 1
//                }
//                while (i < completTable.length) {
//                  val tuple: Array[Int] = Array.tabulate(numberOfTaskToPartition.value)(k => if (k < numberOfTaskToPartition.value - 1) completTable(i)(k) else -1)
//                  incompletTable += tuple
//                  i += 1
//                }
//                improveFinalTable.clear()
//                improveFinalTable ++= incompletTable
//              }
//              numberOfTaskToPartition.value -= 1
//              build = true
//            }
//          }
//          case 2 => while (!build && numberOfTaskToPartition.value < numberOfTask && sizeOfPartition.value < 2000) { // SPVP variation of both parameters
//            currentTableGenerate.clear
//            val tab = new GenerateTable(data, horizon, currentTableGenerate, typeOfTableToGenerate,sizeOfPartition,
//              numberOfTaskToPartition, timeLimitGenerateTable)
//            tab.generateTheTable()
//            if (tab.isComplet()) {
//              finalTable.clear()
//              finalTable ++= currentTableGenerate
//              if (numberOfTaskToPartition.value < numberOfTask) {
//                numberOfTaskToPartition.value += 1
//                sizeOfPartition.value += 1
//              } else {
//                sizeOfPartition.value += 1
//              }
//            } else {
//              if (numberOfTaskToPartition.value < numberOfTask - 1) {
//                numberOfTaskToPartition.value -= 1
//                sizeOfPartition.value -= 1
//              } else {
//                sizeOfPartition.value -= 1
//              }
//              build = true
//            }
//          }
//          case _ => println("Invalid case")
//        }
//      }
//      if (typeOfTableToGenerate == 1 && improveFinalTable.nonEmpty)
//        improveFinalTable
//      else
//        finalTable
//    }
//    def isEqual(a: Array[Int], b: Array[Int]): Boolean = {
//      for (i <- a.indices if a(i) != b(i))
//        return false
//      true
//    }
//
//    def extractTable(incompletTable: ArrayBuffer[Array[Int]], tasksOrdered: Array[Int], lenght: Int): ArrayBuffer[Array[Int]] = {
//      val extractTable: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//      for (a <- incompletTable.indices) {
//        val tuple: Array[Int] = new Array[Int](lenght)
//        for (i <- 0 until lenght) {
//          tuple(i) = incompletTable(a)(tasksOrdered(i))
//        }
//        extractTable += tuple
//      }
//      extractTable
//    }
//}
//
//class GenerateTable(data: RCPSPInstance,
//                    horizon: Int,
//                    tableGenerate: ArrayBuffer[Array[Int]],
//                    typeOfTableToGenerate: Int = 0,
//                    sizeOfPartition: Mut[Int],
//                    numberOfTaskToPartition: Mut[Int],
//                    timeLimitGenerateTable: Mut[Int]) extends CPModel{
//  private var stat: SearchStatistics = null
//  def generateTheTable(): Unit ={
//    val numberOfTask = data.nTask
//    val numberOfResource = data.nRes
//    val Capacity = data.resCapacity
//    val durationData = data.taskDuration
//    val precedence = data.precedence
//    val demandeData = data.taskUsage
//
//    // auxillary variables
//    val starts: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(0 to horizon - durationData(i)))
//    val duration: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(durationData(i)))
//    val ends: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => starts(i) + duration(i))
//
//    // variables
//    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](numberOfTask, sizeOfPartition.value)
//    val bucket: Array[CPIntVar] = new Array[CPIntVar](numberOfTask)
//    val taskSortByTotalEneygy = starts.indices.sortBy(i => -durationData(i)*(0 until numberOfResource).map(r =>demandeData(i)(r)).sum)
//
//    for (i <- taskSortByTotalEneygy) {
//      if(taskSortByTotalEneygy.indexOf(i) < numberOfTaskToPartition.value){
//        val partition = new Partitions(sizeOfPartition.value, starts(i))
//        bounds(i) = partition.nContiguousBound()
//        bucket(i) = CPIntVar(0 until sizeOfPartition.value)
//        add(BucketView(starts(i), bucket(i), bounds(i)))
//      }else{
//        val part = new Partitions(1, starts(i))
//        bounds(i) = part.nContiguousBound()
//        bucket(i) = CPIntVar(0)
//        add(BucketView(starts(i), bucket(i), bounds(i)))
//      }
//    }
//
//    // add constraints
//    // precedence constraint
//    for ((s, e) <- precedence) {
//      add(ends(s) <= starts(e))(solver)
//    }
//    // resource constraint
//    for (r <- 0 until numberOfResource) {
//      val resource: Array[CPIntVar] = Array.tabulate(numberOfTask)(_ => CPIntVar(r)(solver))
//      val height: Array[CPIntVar] = Array.tabulate(numberOfTask)(i => CPIntVar(demandeData(i)(r))(solver))
//      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
//    }
//
//    // On solution
//    onSolution {
//      val tuple: Array[Int] = Array.tabulate(numberOfTask)(i => bucket(i).value)
//      tableGenerate += tuple
//    }
//    // search
//    search {
//      conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
//    }
//    stat = start(timeLimit = timeLimitGenerateTable.value)
//    timeLimitGenerateTable.value -= (stat.time/1000).toInt
//  }
//
//  def isComplet(): Boolean = stat.completed
//}
//
//
