//package rcpsp
//
//import oscar.algo.search.SearchStatistics
//import oscar.cp._
//import oscar.cp.core.CPPropagStrength
//import oscar.cp.scheduling.constraints.MaxCumulative
//import bucket.{BucketView, Partitions}
//
//import scala.collection.mutable.ArrayBuffer
//
//
//object runRCPSP extends CPModel with App {
//  // data
//  val direct: String = "Data/BL/"
//  for(i <- 1 to 20) {
//    val name: String = "bl20_" + i
//    val faileName = direct + name + ".rcp"
//    val data = new RCPSPParser().parser(fileName)
//    val sample = new RCPSPWithBucket(data, 300)
//    val sample1 = new RCPSPWithBucket(data, 300, 3)
//    //val sample2 = new RCPSPWithBucket(data, 300, 7)
//    sample.solve()
//    sample1.solve()
//    //sample2.solve()
//    println(name + " | " + sample.mSpan() + " | " + sample.time() + " | " + sample.tableBuildPostTime() + " | " + sample.nodes() + " | " + sample.fails() + " | "
//      + sample1.mSpan() + " | " + sample1.time() + " | " + sample1.tableBuildPostTime() + " | " + sample1.nodes() + " | " + sample1.fails() + " | "
//      //+ sample2.mSpan() + " | " + sample2.time() + " | " + sample2.tableBuildPostTime() + " | " + sample2.nodes() + " | " + sample2.fails() + " | "
//    )
//  }
//}
//
//
//
//class RCPSPWithBucket (data: RCPSPInstance,
//                               searchTime: Int,
//                               nPart: Int = 1,
//                               silent: Boolean = false) extends CPModel{
//  private val nTask = data.nTask
//  private val nRes = data.nRes
//  private val Capacity = data.resCapacity
//  private val durationData = data.taskDuration
//  private val precedence = data.precedence
//  private val heightData = data.taskUsage
//  private val horizon = durationData.sum
//  private var span: Int = -1
//  // variables
//  private val starts: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
//  private val duration: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
//  private val ends: Array[CPIntVar] = Array.tabulate(nTask)(i => starts(i) + duration(i))
//
//  private var stat: SearchStatistics = null
//  private var buildTable: Double = Double.MinValue
//  def solve(): Unit ={
//    // bucketing the starts variable
//    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart)
//    val bucket: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(0 until nPart))
//    for (i <- starts.indices) {
//      val part = new Partitions(nPart, starts(i))
//      bounds(i) = part.nContiguousBound()
//      //bounds(i) = part.nPartitionOfOnlyBound(15)
//      add(BucketView(starts(i), bucket(i), bounds(i)))
//    }
//
//    // Model
//    for ((s, e) <- precedence) {
//      add(ends(s) <= starts(e))
//    }
//
//    for (r <- 0 until nRes) {
//      val resource: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(r))
//      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r)))
//      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r), CPPropagStrength.Strong)
//    }
//
//    // Objective
//    val makespan = maximum(ends)
//
//
//    val remainingTime: Mut[Int] = new Mut[Int](searchTime)
//
//    val startsTimeForTableGeneration = System.currentTimeMillis()
//
//    val tableForRCPSP: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//    val tab = new TableGenerationForBucket(data: RCPSPInstance, nPart, tableForRCPSP, remainingTime)
//
//    if(tableForRCPSP.nonEmpty){
//      add(table(bucket, tableForRCPSP.toArray))
//    }
//
//    buildTable = (System.currentTimeMillis()- startsTimeForTableGeneration)/1000
//
//    minimize(makespan)
//
//    // On solution
//    onSolution {
//      span = makespan.value
//      if(silent)
//        println(starts.mkString(" , ") + "\nmakespan: " + makespan.value + "\n-----------------------------")
//    }
//    // search
//
//    search{
//      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
//    }
//
//    stat = start(timeLimit = remainingTime.value - buildTable.toInt)
//
//  }
//
//
//
//  def mSpan(): Int = {
//    span
//  }
//
//  def tableBuildPostTime(): Double = {
//    buildTable
//  }
//
//  def nodes(): Int ={
//    if(stat.completed)
//      stat.nNodes
//    else
//      -1
//  }
//
//  def time(): Double ={
//    if(stat.completed)
//      stat.time
//    else
//      -1
//  }
//
//  def fails(): Int ={
//    if(stat.completed)
//      stat.nFails
//    else -1
//  }
//}
//

//
//class TableGenerationForBucket(data: RCPSPInstance, nPart: Int, tableForResource: ArrayBuffer[Array[Int]], remainingTime: Mut[Int]) extends CPModel {
//  private val nTask = data.nTask
//  private val nRes = data.nRes
//  private val Capacity = data.resCapacity
//  private val durationData = data.taskDuration
//  private val precedence = data.precedence
//  private val heightData = data.taskUsage
//  private val horizon = durationData.sum
//  private var span: Int = -1
//  // variables
//  private val starts: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
//  val fakestart = CPIntVar(0 to horizon)
//  private val duration: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
//  private val ends: Array[CPIntVar] = Array.tabulate(nTask)(i => starts(i) + duration(i))
//    // bucketing the starts variable
//    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart)
//    val bucket: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(0 until nPart))
//    for (i <- starts.indices) {
//            val part = new Partitions(nPart, fakestart) //starts(i))
////      val part = new Partitions(nPart, starts(i))
//      bounds(i) = part.nContiguousBound()
//      add(BucketView(starts(i), bucket(i), bounds(i)))
//    }
//
//    // constraint
//    for((i,j) <- precedence) add(ends(i) <= starts(j))
//
//    for(r <- 0 until nRes){
//      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r)))
//      val resource: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(r))
//      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r)), r), CPPropagStrength.Strong)
//    }
//for (i <- ends.indices)
//  add(ends(i) <= 29)
//
//    // search
//    search{
//      conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
////      binaryFirstFail(bucket)
//    }
//
//    // on solution
//    onSolution{
//      val tuple: Array[Int] = Array.tabulate(nTask)(i => bucket(i).value)
//      tableForResource += tuple
//      println(tableForResource.size + " " + tuple.mkString(" - "))
//    }
//    val stat = start(timeLimit = remainingTime.value)
//  println(stat)
//  if(stat.completed)
//    remainingTime.value -= (stat.time/1000).toInt
//}
