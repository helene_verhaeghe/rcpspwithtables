/*******************************************************************************
 * OscaR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * OscaR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with OscaR.
 * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 ******************************************************************************/

import bucket.{BucketViewFixed, PartitionStrategy}
import models.{ModelBucketVars, ModelStartDurationEndVars}
import oscar.cp.{CPSolver, _}
import oscar.cp.core.CPSolver
import bucket._
import oscar.cp._

import scala.collection.mutable.ArrayBuffer
/**
 *  @authors: Pierre Schaus  pschaus@gmail.com
 *  @authors: Renaud Hartert ren.hartert@gmail.com
 */
object RCPSP extends CPModel with App {

  // (duration, consumption)
  val instance = Array((5, 1), (3, 1), (9, 3), (1, 2), (2, 2), (8, 1), (3, 2), (2, 2), (2, 1), (1, 1), (1, 2))
  val durationsData = instance.map(_._1)
  val demandsData = instance.map(_._2)
  val capa = 4
  val nTasks = instance.size
  val Tasks = 0 until nTasks

  val horizon = durationsData.sum
  println("h:",horizon)
  //implicit val cp = CPSolver()

  val durations = Array.tabulate(nTasks)(t => CPIntVar(durationsData(t)))
  val starts = Array.tabulate(nTasks)(t => CPIntVar(0 to horizon - durations(t).min))
  val ends = Array.tabulate(nTasks)(t => starts(t) + durations(t))
  val demands = Array.tabulate(nTasks)(t => CPIntVar(demandsData))
  val resources = Array.tabulate(nTasks)(t => CPIntVar(0))

  val makespan = maximum(ends)

  // Cumulative
  add(maxCumulativeResource(starts, durations, ends, demands, resources, CPIntVar(capa), 0))





  // find initial ub

  var initSolution = true
  var ub: Int = horizon
  onSolution {
    if (initSolution) {
      ub = makespan.value
    }
  }
  search {
    binaryFirstFail(starts)
  }
  initSolution = false



  // create views and collect tuples

  def createScaledViews(starts: Array[CPIntVar], ub: Int): Array[CPIntVar] = {
    val partition = new PartitionEqualButLast(3)
    starts.map(x => {
      val xBucket = partition.getBucket(solver)
      val channel = partition.getBucketView(x, xBucket, ub)
      post(channel)
      xBucket
    })
  }

  val buckets = createScaledViews(starts,ub)


  val tuples: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()

  var collect = true
  onSolution {
    if (collect) {
      val tuple = buckets.map(_.value)
      tuples += tuple
    }
  }

  search {
    binaryFirstFail(buckets)
  }

  solver.start()

  println(tuples.map(_.mkString(",")).mkString("-"))

  collect = false


  println("-------solve with table -------")

  // post redundant table constraint and minimize
  println("redundant table with #"+tuples.size+" tuples")

  minimize(makespan)

  search {
    binaryFirstFail(starts)
  }

  val statsWithTable = solver.startSubjectTo() {
    post(table(buckets,tuples.toArray))
  }
  println("statsWithTable"+statsWithTable)


  solver.obj(makespan).relax()

  println("-------solve without table -------")

  val statsWithoutTable = solver.start()
  println("statsWithTable"+statsWithoutTable)


}
