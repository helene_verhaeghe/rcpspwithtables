package bucket

import oscar.cp.core.Constraint
import oscar.cp.core.delta.DeltaIntVar
import oscar.cp.core.variables.{CPIntVar, CPVar}


object BucketView{
  def apply(variable: CPIntVar, bucket: CPIntVar, bound: Array[(Int, Int)]) =
    new BucketViewBound(variable, bucket, bound)
}

abstract class BucketView(variable: CPIntVar, bucket: CPIntVar) extends Constraint(variable.store){
  val nPart: Int = bucket.size

  // variables containing the delta associated to variable and bucket
  var deltaVariable: DeltaIntVar = null
  var deltaBucket: DeltaIntVar = null

  // temporary variables used with the deltas
  protected val tempArrayVar: Array[Int] = new Array[Int](variable.size)
  protected val tempArrayBucket: Array[Int] = new Array[Int](nPart)
  protected var tempArrayVarSize: Int = 0
  protected var tempArrayBucketSize: Int = 0

  override def associatedVars(): Iterable[CPVar] = Array(variable,bucket)
}

