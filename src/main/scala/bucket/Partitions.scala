package bucket

import oscar.cp.core.variables.CPIntVar

class Partitions(n: Int, variable: CPIntVar) {

  /*
   * Generate the pairs forming the bounds of a partition.
   */
  def nContiguousBound(): Array[(Int, Int)] = {
    val bound: Array[(Int, Int)] = new Array[(Int, Int)](n)
    val p = variable.size / n // rounded down number by partition
    val s = variable.size % n // unassigned
    var prevMin = variable.min
    var nextMin = prevMin
    for (i <- 0 until n) {
      if (i < s) {
        nextMin = prevMin + p
        if (variable.hasValue(prevMin))
          bound(i) = (prevMin, nextMin)
        else
          bound(i) = (prevMin, prevMin - 1)
      } else {
        nextMin = prevMin + p - 1
        if (variable.hasValue(prevMin))
          bound(i) = (prevMin, nextMin)
        else
          bound(i) = (prevMin, prevMin - 1)
      }
      prevMin = nextMin + 1
    }
    bound
  }

}
