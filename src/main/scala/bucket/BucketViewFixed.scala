package bucket

import oscar.algo.reversible.ReversibleInt
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.CPIntVar

class BucketViewFixed(variable: CPIntVar, bucket: CPIntVar, min: Int, nPart: Int, partitionSize: Int) extends BucketView(variable, bucket) {
  // Counters for each partition
  protected val nElemPart: Array[ReversibleInt] = new Array[ReversibleInt](nPart)
  private val bound = Array.tabulate(nPart)(i => (min + i * partitionSize, min - 1 + (i + 1) * partitionSize))
  private val temp = Array.fill(bucket.size)(0)
  private var torm = 0

  override def setup(l: CPPropagStrength): Unit = {
    // check init values of the counters, and two ways init check
    variable.leEq(bound.last._2)
    for (p <- 0 until nPart) {
      if (!bucket.hasValue(p)) {
        for (v <- bound(p)._1 to bound(p)._2)
          variable.removeValue(v)
      } else {
        val count = variable.count(s => bound(p)._1 <= s && s <= bound(p)._2)
        if (count == 0) {
          bucket.removeValue(p)
        } else {
          // init only useful counters
          nElemPart(p) = ReversibleInt(count)(variable.store)
        }
      }
    }


    // init deltas
    deltaVariable = variable.callPropagateOnChangesWithDelta(this)
    deltaBucket = bucket.callPropagateOnChangesWithDelta(this)

    propagate() // TODO check really useful as two way already done?
  }

  override def propagate(): Unit = {
    // propagate changes from bucket to variable
    if (deltaBucket.size > 0) {
      tempArrayBucketSize = deltaBucket.fillArray(tempArrayBucket)
      var i = 0
      while (i < tempArrayBucketSize) {
        val p = tempArrayBucket(i)
        for (v <- bound(p)._1 to bound(p)._2) {
          variable.removeValue(v)
        }
        i += 1
      }
    }
    // propagate changes from variable to bucket
    if (deltaVariable.size > 0) {
      tempArrayVarSize = deltaVariable.fillArray(tempArrayVar)
      var i = 0
      while (i < tempArrayVarSize) {
//        println(tempArrayVar(i) + " " + min + " " + partitionSize)
        nElemPart((tempArrayVar(i) - min) / partitionSize).decr()
        i += 1
      }
//      for (p <- 0 until nPart) {// TODO should be replaced by the code below, when push from guillaume is done
//        if (bucket.hasValue(p) && nElemPart(p).value == 0) {
//          bucket.removeValue(p)
//        }
//      }
      torm = 0
      bucket.foreach{p =>
        if (nElemPart(p).value == 0) {
          temp(torm)=p
          torm+=1
        }
      }
      while (torm > 0){
        torm -=1
        bucket.removeValue(temp(torm))
      }
      //      bucket.foreach{p =>
      //        if (nElemPart(p).value == 0) {
      //          bucket.removeValue(p)
      //        }
      //      }
    }

  }
}
