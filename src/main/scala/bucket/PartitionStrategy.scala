package bucket

import oscar.cp._

/**
 * Help partition the horizon and create bucket variable and corresponding channeling constraint
 */
abstract class PartitionStrategy {

  val nPart:Int

  def getBucket(solver:CPSolver) :CPIntVar

  def getBucketView(variable:CPIntVar,bucket:CPIntVar,horizon:Int) :Constraint

}

object PartitionStrategy{
  def apply(str:String, nPart:Int) = {
    str match {
      case "eqButLast" => new PartitionEqualButLast(nPart)
      case "eqMod" => new PartitionEqualMod(nPart)
      case "eqButLastTAB" => new PartitionEqualButLastTable(nPart)
      case "eqModTAB" => new PartitionEqualModTable(nPart)
    }
  }
}