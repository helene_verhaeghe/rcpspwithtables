package bucket

import oscar.cp._

/**
 * Partition of the domain into equal partition. If domain not multiple of nPart, last part is smaller
 *
 * Ex : domain size of 10, nPart = 3, partition are 4, 4, 2
 */
class PartitionEqualButLast(val nPart: Int) extends PartitionStrategy {

  def getBucket(solver: CPSolver): CPIntVar = {
    CPIntVar(0 until nPart)(solver)
  }

  def getBucketView(variable: CPIntVar, bucket: CPIntVar, horizon: Int): Constraint = {
    val partitionSize = 1 + horizon / nPart
    new BucketViewFixed(variable, bucket, 0, nPart, partitionSize)
  }
}

/**
 * Partition of the domain into equal partition. If domain not multiple of nPart, last part is smaller
 *
 * Ex : domain size of 10, nPart = 3, partition are 4, 4, 2
 */
class PartitionEqualButLastTable(val nPart: Int) extends PartitionStrategy {

  def getBucket(solver: CPSolver): CPIntVar = {
    CPIntVar(0 until nPart)(solver)
  }

  def getBucketView(variable: CPIntVar, bucket: CPIntVar, horizon: Int): Constraint = {
    val partitionSize = 1 + horizon / nPart
    val par = Array.tabulate(nPart)(i => (i * partitionSize, Math.min(1 + (i + 1) * partitionSize,horizon)))
    val tab:Array[Array[Int]] = Array.fill(horizon+1)(null)
    for (i <- par.indices){
      val p =par(i)
      for (j <- p._1 to p._2){
        tab(j) = Array(i,j)
      }
    }
    table(Array(bucket,variable),tab)
  }
}

/**
 * Partition of the domain into equal partition. If domain not multiple of nPart, even all partition
 *
 * Ex : domain size of 10, nPart = 3, partition are 4, 3, 3
 */
class PartitionEqualMod(val nPart: Int) extends PartitionStrategy {

  def getBucket(solver: CPSolver): CPIntVar = {
    CPIntVar(0 until nPart)(solver)
  }

  def getBucketView(variable: CPIntVar, bucket: CPIntVar, horizon: Int): Constraint = {
    val partition = new Partitions(nPart, CPIntVar(0 to horizon)(variable.store)) //TODO
    new BucketViewBound(variable, bucket, partition.nContiguousBound())
  }
}

/**
 * Partition of the domain into equal partition. If domain not multiple of nPart, even all partition
 *
 * Ex : domain size of 10, nPart = 3, partition are 4, 3, 3
 */
class PartitionEqualModTable(val nPart: Int) extends PartitionStrategy {

  def getBucket(solver: CPSolver): CPIntVar = {
    CPIntVar(0 until nPart)(solver)
  }

  def getBucketView(variable: CPIntVar, bucket: CPIntVar, horizon: Int): Constraint = {
    val partition = new Partitions(nPart, CPIntVar(0 to horizon)(variable.store))
    val par = partition.nContiguousBound()
    val tab:Array[Array[Int]] = Array.fill(horizon+1)(null)
    for (i <- par.indices){
      val p =par(i)
      for (j <- p._1 to p._2){
        tab(j) = Array(i,j)
      }
    }
    table(Array(bucket,variable),tab)
  }
}

object tttootot extends App {

}