package bucket

import oscar.cp.core.variables.CPIntVar


object Bucket {

  /*
   * Create a variable used as bucket and the associated bucket constraint
   * The size of the partition is given as input
   */
  def generateFixedSize(variable: CPIntVar, min: Int, max: Int, partitionSize: Int): (CPIntVar, BucketView) = {
    val nPart = 1 + (max - min) / partitionSize
    generate(variable, min, nPart, partitionSize)
  }

  /*
   * Create a variable used as bucket and the associated bucket constraint
   * The number of partition is given is of fixed size
   */
  def generateFixedNumber(variable: CPIntVar, min: Int, max: Int, nPart: Int): (CPIntVar, BucketView) = {
    val partitionSize = 1 + (max - min) / nPart
    generate(variable, min, nPart, partitionSize)
  }


  private def generate(variable: CPIntVar, min: Int, nPart: Int, partitionSize: Int): (CPIntVar, BucketView) = {
    val bucket = CPIntVar(0, nPart)(variable.store)
    val cst = new BucketViewFixed(variable, bucket,min, nPart, partitionSize)
    variable.store.add(cst)
    (bucket, cst)
  }


}
