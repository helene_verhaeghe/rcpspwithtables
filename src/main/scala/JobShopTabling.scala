/*******************************************************************************
 * OscaR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * OscaR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with OscaR.
 * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 ******************************************************************************/

import bucket.PartitionEqualButLast
import oscar.cp._
import oscar.util.RandomGenerator

import scala.collection.mutable.ArrayBuffer
import scala.io.Source


/**
 * Job-Shop Problem
 *
 *  A Job is a a sequence of n Activities that must be executed one after the
 *  others. There are n machines and each activity of the jobs require one of the
 *  n machines. The objective is to assign the starting time of each activity
 *  minimizing the total makespan and such that no two activities from two different
 *  jobs requiring the same machine overlap.
 *
 *  @author Pierre Schaus  pschaus@gmail.com
 *  @author Renaud Hartert ren.hartert@gmail.com
 *  @author Cyrille Dejemeppe cyrille.dejemeppe@gmail.com
 */
object JobShopTabling extends App {

  // Parsing    
  // -----------------------------------------------------------------------

  var lines = Source.fromFile("data/ft07.txt").getLines().toList

  val nJobs = lines.head.trim().split(" ")(0).toInt
  val nTasksPerJob = lines.head.trim().split(" ")(1).toInt
  val nResources = lines.head.trim().split(" ")(2).toInt

  val nActivities = nJobs * nTasksPerJob

  val Activities = 0 until nActivities
  val Jobs = 0 until nJobs
  val Resources = 0 until nResources

  lines = lines.drop(1)

  val jobs = Array.fill(nActivities)(0)
  val resources = Array.fill(nActivities)(0)
  val durations = Array.fill(nActivities)(0)

  for (i <- Activities) {

    val l = lines.head.trim().split("[ ,\t]+").map(_.toInt)

    jobs(i) = l(0)
    resources(i) = l(1)
    durations(i) = l(2)

    lines = lines.drop(1)
  }

  // Modeling 
  // -----------------------------------------------------------------------

  implicit val cp = CPSolver()
  val horizon = durations.sum

  // Activities & Resources
  val durationsVar = Array.tabulate(nActivities)(t => CPIntVar(durations(t)))
  val startsVar = Array.tabulate(nActivities)(t => CPIntVar(0 to horizon - durationsVar(t).min))
  val endsVar = Array.tabulate(nActivities)(t => startsVar(t)+durations(t))
  val demandsVar = Array.fill(nActivities)(CPIntVar(1))
  val resourcesVar = Array.tabulate(nActivities)(t => CPIntVar(resources(t)))

  val makespan = maximum(endsVar)

  val bestSolutionStarts = Array.ofDim[Int](nActivities)



  // Precedences
  for (t <- 1 to Activities.max if jobs(t - 1) == jobs(t)) {
    add(endsVar(t - 1) <= startsVar(t))
  }
  // Cumulative
  val rankBranchings = for (r <- Resources) yield {
    def filter(x: Array[CPIntVar]) = Activities.filter(resources(_) == r).map(x(_))
    val (s,d,e) = (filter(startsVar), filter(durationsVar), filter(endsVar))
    add(unaryResource(s,d,e))
    rank(s,d,e)
  }

  minimize(makespan)


  // -------------------------



  // find initial ub

  var initSolution = true
  var ub: Int = horizon
  onSolution {
    if (initSolution) {
      ub = makespan.value
    }
  }
  search {
    binaryFirstFail(startsVar)
  }
  initSolution = false

  start(nSols = 1)
  //
  cp.post(makespan <= ub)



  // --------------------------------

  println("-------solve without table -------")
  cp.obj(makespan).relax()
  search {
    conflictOrderingSearch(startsVar,i => startsVar(i).size, i => startsVar(i).min)
  }

  val statsWithoutTable = cp.start()
  println("statsWithoutTable"+statsWithoutTable)



  // ----------------------------

  cp.obj(makespan).relax()
  cp.obj(makespan).tightenMode = TightenType.NoTighten

  // create views and collect tuples

  def createScaledViews(starts: Array[CPIntVar], ub: Int): Array[CPIntVar] = {
    val partition = new PartitionEqualButLast(5)
    starts.map(x => {
      val xBucket = partition.getBucket(cp)
      val channel = partition.getBucketView(x, xBucket, ub)
      post(channel)
      xBucket
    })
  }

  val buckets = createScaledViews(startsVar.take(30),ub)


  val tuples: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()

  var collect = true
  onSolution {
    if (collect) {
      val tuple = buckets.map(_.value)
      tuples += tuple
      if (tuples.length % 1000 == 0) {
        println(tuples.length)
      }
    }
  }

  search {
    conflictOrderingSearch(buckets,i => buckets(i).size, i => buckets(i).min)
  }

  cp.start()


  collect = false



  println("-------solve with table -------")
  cp.obj(makespan).relax()

  cp.obj(makespan).tightenMode = TightenType.StrongTighten

  // post redundant table constraint and minimize
  println("redundant table with #"+tuples.size+" tuples")


  search {
    conflictOrderingSearch(startsVar,i => startsVar(i).size, i => startsVar(i).min)
  }

  val statsWithTable = cp.startSubjectTo() {
    post(table(buckets,tuples.toArray))
  }
  println("statsWithTable"+statsWithTable)






  /*
  minimize(makespan) 

  val rankBranching = rankBranchings.reduce{_++_}
    
  search {
    //conflictOrderingSearch(startsVar,i => startsVar(i).size, i => startsVar(i).min)
     rankBranchings.reduce{_++_} ++ binaryStatic(startsVar)
  }
  val stats = start()
  println(stats)
  */





}


