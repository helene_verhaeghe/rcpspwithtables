package solve


import java.io.{File, FileOutputStream, PrintWriter}
import scala.io.Source

object ProcessInitMed extends App {

  val outputdir = args(0)

  def getpw(file: String) =
    new PrintWriter(new FileOutputStream(new File(outputdir + "/" + file), true))

  var str1 = "inst"
  var str2 = "\t"

 val iter = Array(
    ("res_init", "INIT", "init"),
    ("res_ft_2", "FullTable 2", "ft 2"),
    ("res_ft_3", "FullTable 3", "ft 3"),
    ("res_fixed_5_2", "Fixed 5 2", "fix 5 2"),
    ("res_fixed_5_3", "Fixed 5 3", "fix 5 3"),
    ("res_fixed_10_2", "Fixed 10 2", "fix 10 2"),
    ("res_fixed_10_3", "Fixed 10 3", "fix 10 3"),
    ("res_fixed_15_2", "Fixed 15 2", "fix 15 2"),
    ("res_fixed_15_3", "Fixed 15 3", "fix 15 3"),
    ("res_fixed_20_2", "Fixed 20 2", "fix 20 2"),
    ("res_fixed_20_3", "Fixed 20 3", "fix 20 3"),
    ("res_fixed_vb", "Fixed vb", "fix vb"),
   ("res_fixed_vb_step3", "Fixed vb s3", "fix vb s3"),
    ("res_iter_5_5_2", "Iteratif 5 5 2", "iter 5 5 2"),
    ("res_iter_10_5_2", "Iteratif 10 5 2", "iter 10 5 2"),
   ("res_iter_5_10_2", "Iteratif 5 10 2", "iter 5 10 2"),
   ("res_iter_10_10_2", "Iteratif 10 10 2", "iter 10 10 2"),
   ("res_iter_5_15_2", "Iteratif 5 15 2", "iter 5 15 2"),
   ("res_iter_10_15_2", "Iteratif 10 15 2", "iter 10 15 2"),
   ("res_iter_5_20_2", "Iteratif 5 20 2", "iter 5 20 2"),
   ("res_iter_10_20_2", "Iteratif 10 20 2", "iter 10 20 2"),
    ("res_iter_vb", "Iter vb", "iter vb"),
    ("res_iterFB_5_5_2", "Iteratif Feedback 5 5 2", "iterFB 5 5 2"),
    ("res_iterFB_10_5_2", "Iteratif Feedback 10 5 2", "iterFB 10 5 2"),
   ("res_iterFB_5_10_2", "Iteratif Feedback 5 10 2", "iterFB 5 10 2"),
   ("res_iterFB_10_10_2", "Iteratif Feedback 10 10 2", "iterFB 10 10 2"),
   ("res_iterFB_5_15_2", "Iteratif Feedback 5 15 2", "iterFB 5 15 2"),
   ("res_iterFB_10_15_2", "Iteratif Feedback 10 15 2", "iterFB 10 15 2"),
   ("res_iterFB_5_20_2", "Iteratif Feedback 5 20 2", "iterFB 5 20 2"),
   ("res_iterFB_10_20_2", "Iteratif Feedback 10 20 2", "iterFB 10 20 2"),
    ("res_iterFB_vb", "Iter FB vb", "iterFB vb")
  )

  def printHeader(file: String, algoname: String, algoshortname: String) = {
    val pw_ = getpw(file)
    pw_.println("---")
    pw_.println("algname: " + algoname)
    pw_.println("success: OK")
    pw_.println("free_format: True")
    pw_.println("---")
    pw_.close()
    str1 += "\t" + algoshortname + "\t"
    str2 += "\tobj\ttime"
  }

  for ((file, algoname, algoshortname) <- iter) {
    printHeader(file, algoname, algoshortname)
  }

  val pw = new PrintWriter(new FileOutputStream(new File(outputdir + "/outputMed.txt"), true))
  pw.println(str1)
  pw.println(str2)
  pw.close()
}

object ProcessMed extends App {

  val outputfile = "outputMed.txt"

  def getpw(file: String) = {
    val pw = new PrintWriter(new FileOutputStream(new File(outputdir + "/" + file), true))
    pw
  }

  val instanceName = args(0)
  val instanceNameNoU = instanceName.replace('_', '-')
  val inputdir = args(1)
  val outputdir = args(2)
  val TO = args(3).toInt

  var bestHorizon = -1
  var firstHorizon = -1
  var firstCompleted = false
  var inittime = TO


  val s_init = Array("init")
  val s_ft = Array(2, 3).map(i => "ft_" + i)
  val s_fixed = Array(5, 10, 15, 20).flatMap(i =>
    Array("fixed_" + i + "_2", "fixed_" + i + "_3")
  )

  val s_iter = Array(5, 10, 15, 20).flatMap(i=> Array("iter_5_"+i+"_2", "iter_10_"+i+"_2"))
  val s_iterFB = Array(5, 10, 15, 20).flatMap(i=> Array("iterFB_5_"+i+"_2", "iterFB_10_"+i+"_2"))


  val pw = getpw(outputfile)
  val pw_title = getpw("data.subset")

  pw.print(instanceName + "\t")
  pw_title.println(instanceNameNoU)
  pw_title.close()

  def processTime(time: Long, horizon: Int, pw_pp: PrintWriter, completed: Boolean, TO: Int = 900000) = {
    if (completed) {
      pw.print(horizon + "\t" + time + "\t")
      pw_pp.println(instanceNameNoU + " OK " + time)
    } else {
      pw.print(horizon + "\tTO\t")
      pw_pp.println(instanceNameNoU + " TO " + TO)
    }
  }

  s_init.foreach { x =>
    val pw = getpw("res_" + x)
    //init
    val lines = Source.fromFile(inputdir + "/" + instanceName + "_" + x + ".txt").getLines
    var results = false
    var horizon = -1
    var time = -1
    var completed = false
    for (line <- lines) {
      results |= line.contains("Search for all solution of P (COS): End")
      if (results) {
        if (line.contains("Total time (with preproc) (ms): ")) {
          time = line.replace("Total time (with preproc) (ms): ", "").toInt
          inittime = time
        } else if (line.contains("Horizon :")) {
          horizon = line.replace("Horizon :\t", "").toInt
        } else if (line.contains("completed: ")) {
          completed = line.replace("completed: ", "").toBoolean
        }
      }
    }
    processTime(time, horizon, pw, completed, TO)
    if (completed) {
      bestHorizon = horizon
      firstCompleted = true
    }

    pw.close()

  }

  s_ft.foreach { x =>
    val pw = getpw("res_" + x)
    //ft
    val lines = Source.fromFile(inputdir + "/" + instanceName + "_" + x + ".txt").getLines
    var results = false
    var horizon = -1
    var time = -1
    var completed = false
    var rfirst = false
    for (line <- lines) {
      results |= line.contains("Search for all solution of P+T (COS): End")
      if (results) {
        if (line.contains("Total time (with preproc) (ms): ")) {
          time = line.replace("Total time (with preproc) (ms): ", "").toInt
        } else if (line.contains("Horizon :")) {
          horizon = line.replace("Horizon :\t", "").toInt
        } else if (line.contains("completed: ")) {
          completed = line.replace("completed: ", "").toBoolean
        }
      }
      if (line.contains("Search for first solution of P (COS) : Best Solution")) {
        rfirst = true
      } else if (rfirst && line.contains("Horizon :") && firstHorizon == -1) {
        firstHorizon = line.replace("Horizon :\t", "").toInt
        rfirst = false
      }
    }
    processTime(time, horizon, pw, completed, TO)
    if (horizon != -1 &&bestHorizon != -1){
      if (firstCompleted) {
        if (completed && horizon != bestHorizon) {
          println("NOT RIGHT OPTIM " + instanceName + " " + x + " ("+bestHorizon + " " + horizon+")")
        }
        if (horizon < bestHorizon) {
          println("BETTER THAN OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      } else {
        if (completed && horizon > bestHorizon) {
          println("NOT RIGHT OPTIM2 " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      }
    }
    pw.close()
  }


  var vb_fixed = TO
  var vb_fixed_step3 = TO
  var vb_fixed_horizon = Int.MaxValue
  var vb_fixed_completed = false

  s_fixed.map{x =>
    val pw = getpw("res_" + x)
    //fixed
    val lines = Source.fromFile(inputdir + "/" + instanceName + "_" + x + ".txt").getLines
    var results = false
    var horizon = -1
    var time = -1
    var time_step3 = -1
    var completed = false
    for (line <- lines) {
      results |= line.contains("Search for all solution of P+T (COS): End")
      if (results) {
        if (line.contains("Total time (with preproc) (ms): ")) {
          time = line.replace("Total time (with preproc) (ms): ", "").toInt
        } else if (line.contains("Horizon :")) {
          horizon = line.replace("Horizon :\t", "").toInt
        } else if (line.contains("completed: ")) {
          completed = line.replace("completed: ", "").toBoolean
        } else if (line.contains("Step 3 time (with preproc) (ms): ")){
          time_step3 = line.replace("Step 3 time (with preproc) (ms): ","").toInt
        }
      }
    }
    processTime(time,horizon,pw,completed,TO)
    if (horizon != -1 &&bestHorizon != -1){
      if (firstCompleted) {
        if (completed && horizon != bestHorizon) {
          println("NOT RIGHT OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
        if (horizon < bestHorizon) {
          println("BETTER THAN OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      } else {
        if (completed && horizon > bestHorizon) {
          println("NOT RIGHT OPTIM2 " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      }
    }
    if (time < vb_fixed && completed) {
      vb_fixed = time
      vb_fixed_step3 = time_step3
      vb_fixed_horizon = horizon
      vb_fixed_completed = true
    }
    pw.close()
  }

  val pw_fixed_vb = getpw("res_fixed_vb")
  processTime(vb_fixed,vb_fixed_horizon,pw_fixed_vb,vb_fixed_completed,TO)
  pw_fixed_vb.close()

  val pw_fixed_vb_step3 = getpw("res_fixed_vb_step3")
  processTime(vb_fixed_step3,vb_fixed_horizon,pw_fixed_vb_step3,vb_fixed_completed,TO)
  pw_fixed_vb_step3.close()

  var vb_iter = TO
  var vb_iter_horizon = Int.MaxValue
  var vb_iter_completed = false
  s_iter.map{x =>
    val pw = getpw("res_" + x)
    //fixed
    val lines = Source.fromFile(inputdir + "/" + instanceName + "_" + x + ".txt").getLines
    var results = false
    var horizon = -1
    var time = -1
    var completed = false
    for (line <- lines) {
      results |= line.contains("Search for all solution of P+T (COS): End")
      if (results) {
        if (line.contains("Total time (with preproc) (ms): ")) {
          time = line.replace("Total time (with preproc) (ms): ", "").toInt
        } else if (line.contains("Horizon :")) {
          horizon = line.replace("Horizon :\t", "").toInt
        } else if (line.contains("completed: ")) {
          completed = line.replace("completed: ", "").toBoolean
        }
      }
      if (line.contains("The table is not complete") && inittime > 2000){
        val pwE = new PrintWriter(new FileOutputStream(new File(outputdir + "/maybeWrong"), true))
        pwE.println(instanceName + " iter " + x)
        pwE.close()
      }
    }
    processTime(time,horizon,pw,completed,TO)
    if (horizon != -1 &&bestHorizon != -1){
      if (firstCompleted) {
        if (completed && horizon != bestHorizon) {
          println("NOT RIGHT OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
        if (horizon < bestHorizon) {
          println("BETTER THAN OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      } else {
        if (completed && horizon > bestHorizon) {
          println("NOT RIGHT OPTIM2 " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      }
    }
    if (time < vb_iter && completed) {
      vb_iter = time
      vb_iter_horizon = horizon
      vb_iter_completed = true
    }
    pw.close()
  }

  val pw_iter_vb = getpw("res_iter_vb")
  processTime(vb_iter,vb_iter_horizon,pw_iter_vb,vb_iter_completed,TO)
  pw_iter_vb.close()

  var vb_iterFB = TO
  var vb_iterFB_horizon = Int.MaxValue
  var vb_iterFB_completed = false
  s_iterFB.map{x =>
    val pw = getpw("res_" + x)
    //fixed
    val lines = Source.fromFile(inputdir + "/" + instanceName + "_" + x + ".txt").getLines
    var results = false
    var horizon = -1
    var time = -1
    var completed = false
    for (line <- lines) {
      results |= line.contains("Search for all solution of P+T (COS): End")
      if (results) {
        if (line.contains("Total time (with preproc) (ms): ")) {
          time = line.replace("Total time (with preproc) (ms): ", "").toInt
        } else if (line.contains("Horizon :")) {
          horizon = line.replace("Horizon :\t", "").toInt
        } else if (line.contains("completed: ")) {
          completed = line.replace("completed: ", "").toBoolean
        }
      }
      if (line.contains("The table is not complete") && inittime > 2000){
        val pwE = new PrintWriter(new FileOutputStream(new File(outputdir + "/maybeWrong"), true))
        pwE.println(instanceName + " iter " + x)
        pwE.close()
      }
    }
    processTime(time,horizon,pw,completed,TO)
    if (bestHorizon == -1 && completed){
      println("------------------------------------------------> FLAG")
    }

    if (horizon != -1 && bestHorizon != -1 ){
      if (firstCompleted) {
        if (completed && horizon != bestHorizon) {
          println("NOT RIGHT OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
        if (horizon != -1 && horizon < bestHorizon) {
          println("BETTER THAN OPTIM " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      } else {
        if (completed && horizon > bestHorizon) {
          println("NOT RIGHT OPTIM2 " + instanceName + " " + x+ " ("+bestHorizon + " " + horizon+")")
        }
      }
    }
    if (time < vb_iterFB && completed) {
      vb_iterFB = time
      vb_iterFB_horizon = horizon
      vb_iterFB_completed = true
    }
    pw.close()
  }

  val pw_iterFB_vb = getpw("res_iterFB_vb")
  processTime(vb_iterFB,vb_iterFB_horizon,pw_iterFB_vb,vb_iterFB_completed,TO)
  pw_iterFB_vb.close()

  pw.close()

  if (inittime > 2000) {
    val pw_optim = new PrintWriter(new FileOutputStream(new File(outputdir + "/data_atLeast2sec.subset"), true))
    pw_optim.println(instanceNameNoU)
    pw_optim.close()
    if (bestHorizon != -1 && bestHorizon == firstHorizon) {
      val pw_optim1 = new PrintWriter(new FileOutputStream(new File(outputdir + "/data_atLeast2sec_optimfirst.subset"), true))
      pw_optim1.println(instanceNameNoU)
      pw_optim1.close()
    } else {
      val pw_optim1 = new PrintWriter(new FileOutputStream(new File(outputdir + "/data_atLeast2sec_optimnotfirst.subset"), true))
      pw_optim1.println(instanceNameNoU)
      pw_optim1.close()
    }
  }
  if (inittime > 5000) {
    val pw_optim = new PrintWriter(new FileOutputStream(new File(outputdir + "/data_atLeast5sec.subset"), true))
    pw_optim.println(instanceNameNoU)
    pw_optim.close()
  }

}
