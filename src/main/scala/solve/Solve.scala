package solve

import bucket.PartitionEqualButLast
import models.{Energy, Task}
import rcpsp._


//object Solve extends App {
//  val direcory: String = "./data/BL/"
//  for (i <- 1 to 1) {
//    val name: String = "bl25_" + i
//    val data = new RCPSPParser().parser(direcory + name + ".rcp")
//    val horizon = new SearchForFirstSolutionSpan(data).firstSolutionMakeSpan()
//    val maxTypeOfTable = 3
//    for (typ <- 0 until maxTypeOfTable) {
//      val pb = new RCPSPWithTableFixed(data, horizon, 10, new Mut[Int](30), typ)
//      pb.solveTheProblem()
//    }
//  }
//
//
//
//}


object SolveOne extends App {

  val data = new RCPSPParser().parser("./data/BL/bl25_1.rcp")

  def solve(data: RCPSPInstance) = {
    var remainingTime = 10000

    val data = new RCPSPParser().parser("./data/BL/bl25_1.rcp")

    val x = Task.initProblem_solveToOptimal(data,remainingTime)

    // Step 1: find one solution to reduce the horizon
    val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime)
    remainingTime -= (statFirst.time / 1000).toInt

    // Step 2: create table using the first sol horizon (help reduce the table size)
    val nPart = 2
    val partStrat = new PartitionEqualButLast(nPart)
//    val (table, statTab) = Task.generateTable_tableForAllTasks(data, remainingTime, horizon, partStrat)
    val energy = new Energy(data)
    val index = energy.getNTask(5)
    val (table, statTab) = Task.generateTable_tableForSomeTasks(data, remainingTime, horizon, partStrat, index)
    remainingTime -= (statFirst.time / 1000).toInt


    // Step 3: Solve the problem with the table as redundant constraint
    val (bestHorizon,statSol) = Task.problemWithTable_solveToOptimal(data, remainingTime,table, horizon, partStrat)

    println("Total time (s): " + ((statFirst.time + statTab.time + statSol.time) / 1000.0))
  }

  solve(data)
}


object SolveAll extends App {
  val data = new RCPSPParser().parser("./data/BL/bl25_20.rcp")

  def solve(data: RCPSPInstance) = {
    val remainingTime: Mut[Int] = new Mut[Int](10000)

    // Step 1: find one solution to reduce the horizon
    val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime.value)
    remainingTime.value -= (statFirst.time / 1000).toInt


    val remainingTimeFVP: Mut[Int] = new Mut[Int](30)
    val nPartFVP: Mut[Int] = new Mut[Int](4)
    val nTaskToPartFVP: Mut[Int] = new Mut[Int](10)

    val remainingTimeFSP: Mut[Int] = new Mut[Int](30)
    val nPartFSP: Mut[Int] = new Mut[Int](10)
    val nTaskToPartFSP: Mut[Int] = new Mut[Int](4)

    val remainingTimeSPVP: Mut[Int] = new Mut[Int](30)
    val nPartSPVP: Mut[Int] = new Mut[Int](7)
    val nTaskToPartSPVP: Mut[Int] = new Mut[Int](7)


    /*val (statBasicReplay, statSPVPReplay, statFVPReplay, statFSPReplay) = SolveWithTableReplay(data, horizon, 60,
    remainingTimeSPVP, nPartSPVP, nTaskToPartSPVP,
    remainingTimeFVP, nPartFVP, nTaskToPartFVP,
    remainingTimeFSP, nPartFSP, nTaskToPartFSP)*/

    val (statBasicRestart, statSPVPRestart, statFVPRestart, statFSPRestart) = SolveWithTableRestart(data, horizon, 60,
      remainingTimeSPVP, nPartSPVP, nTaskToPartSPVP,
      remainingTimeFVP, nPartFVP, nTaskToPartFVP,
      remainingTimeFSP, nPartFSP, nTaskToPartFSP)
  }

  solve(data)
}