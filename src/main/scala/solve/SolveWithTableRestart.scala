package solve

import bucket.{BucketView, Partitions}
import oscar.algo.search.SearchStatistics
import oscar.cp._
import oscar.cp.core.{CPPropagStrength, CPSolver}
import oscar.cp.scheduling.constraints.MaxCumulative
import rcpsp.{Mut, RCPSPInstance}

object SolveWithTableRestart {
  def apply(data: RCPSPInstance, horizon: Int, remainingTime: Int,
            remainingTimeSPVP: Mut[Int],  nPartSPVP: Mut[Int], nTaskToPartSPVP: Mut[Int],
            remainingTimeFVP: Mut[Int],  nPartFVP: Mut[Int], nTaskToPartFVP: Mut[Int],
            remainingTimeFSP: Mut[Int],  nPartFSP: Mut[Int], nTaskToPartFSP: Mut[Int]):(SearchStatistics, SearchStatistics, SearchStatistics, SearchStatistics) = {
    implicit val solver = CPSolver()
    val nTask = data.nTask
    val nRes = data.nRes
    val Capacity = data.resCapacity
    val durationData = data.taskDuration
    val precedence = data.precedence
    val heightData = data.taskUsage

    // variables
    val starts: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
    val duration: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
    val ends: Array[CPIntVar] =
      Array.tabulate(nTask)(i => starts(i) + duration(i))
    // sort task by total energy
    val taskSortedByEnergy: Array[Int] = (0 until nTask).sortBy(i => -durationData(i) * (0 until nRes).map(r => heightData(i)(r)).sum)
    val fakestart = CPIntVar(0 to horizon)

    // build bucket variable for the replay of SPVP model
    val (tableSPVP, statTableSPVP) = GenerateTableSPVP(data, horizon, remainingTimeSPVP, nPartSPVP, nTaskToPartSPVP)
    // bucketing the starts variable
    val boundsSPVP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartSPVP.value)
    val bucketSPVP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartSPVP.value){
        val part = new Partitions(nPartSPVP.value, fakestart)
        bucketSPVP(i) = CPIntVar(0 until nPartSPVP.value)(solver)
        boundsSPVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketSPVP(i), boundsSPVP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketSPVP(i) = CPIntVar(0)(solver)
        boundsSPVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketSPVP(i), boundsSPVP(i)))
      }
    }

    // build bucket variable for the replay of FVP model
    val (tableFVP, statTableFVP) = GenerateTableFVP(data, horizon, remainingTimeFVP, nPartFVP, nTaskToPartFVP)
    // bucketing the starts variable
    val boundsFVP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartFVP.value)
    val bucketFVP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartFVP.value){
        val part = new Partitions(nPartFVP.value, fakestart)
        bucketFVP(i) = CPIntVar(0 until nPartFVP.value)(solver)
        boundsFVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFVP(i), boundsFVP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketFVP(i) = CPIntVar(0)(solver)
        boundsFVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFVP(i), boundsFVP(i)))
      }
    }
    // build bucket variable for the replay of FSP model
    val (tableFSP, statTableFSP, isShortTable) = GenerateTableFSP(data, horizon, remainingTimeFSP, nPartFSP, nTaskToPartFSP)
    // bucketing the starts variable
    val boundsFSP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartFSP.value)
    val bucketFSP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartFSP.value){
        val part = new Partitions(nPartFSP.value, fakestart)
        bucketFSP(i) = CPIntVar(0 until nPartFSP.value)(solver)
        boundsFSP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFSP(i), boundsFSP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketFSP(i) = CPIntVar(0)(solver)
        boundsFSP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFSP(i), boundsFSP(i)))
      }
    }

    // constraint
    for ((i, j) <- precedence)
      add(ends(i) <= starts(j))

    for (r <- 0 until nRes) {
      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
    }

    val makespan = maximum(ends)

    minimize(makespan)(solver)

    // search
    search {
      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
    }

    // on solution
    onSolution {
      println("start: " + starts.mkString(" - ")  +"\nmakeSpan: " + makespan)
    }
    println("basic model without table for restart")
    val statBasicRestart = start(timeLimit = remainingTime)
    println(statBasicRestart)
    println("restart with table SPVP")
    val statSPVPRestart = startSubjectTo(timeLimit = remainingTime*2){
      println(tableSPVP.length)
      solver.objective.objs.head.relax()
      if(tableSPVP.nonEmpty)
        add(table(bucketSPVP, tableSPVP))
    }
    println(statSPVPRestart)
    println("replay with table FVP")
    val statFVPRestart = startSubjectTo(timeLimit = remainingTime*2){
      println(tableFVP.length)
      solver.objective.objs.head.relax()
      if(tableFVP.nonEmpty)
        add(table(bucketFVP, tableFVP))
    }
    println(statFVPRestart)
    println("restart with table FSP")
    val statFSPRestart = startSubjectTo(timeLimit = remainingTime*2){
      println(tableFSP.length)
      solver.objective.objs.head.relax()
      if(tableFSP.nonEmpty){
        if(isShortTable){
          val partialBucketFSP: Array[CPIntVar] = Array.tabulate(nTaskToPartFSP.value)(i => bucketFSP(taskSortedByEnergy(i)))
          add(shortTable(partialBucketFSP, tableFSP))
        }else{
          add(table(bucketFSP, tableFSP))
        }
      }
    }
    println(statFSPRestart)
    (statBasicRestart, statSPVPRestart, statFVPRestart, statFSPRestart)
  }
}
