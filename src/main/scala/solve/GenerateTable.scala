//package solve
//
//import bucket.PartitionEqualButLast
//import models.ModelGenerateTable
//import rcpsp.{Mut, RCPSPInstance}
//
//object GenerateTable {
//
//  def apply(data: RCPSPInstance, remainingTime: Mut[Int], horizon: Int, nPart: Int) = {
//    val model = new ModelGenerateTable(data,Array.tabulate(data.nTask)(i=> i),horizon,new PartitionEqualButLast(nPart))
//    model.loadModel
//    val stat = model.startBestSol(remainingTime.value)
//    println(stat)
//    if (!stat.completed)
//      throw new Exception("The table is not complete")
//    (model.getTable, stat)
//
//
//    //    implicit val solver = CPSolver()
////    val nTask = data.nTask
////    val nRes = data.nRes
////    val Capacity = data.Capacity
////    val durationData = data.duration
////    val precedence = data.precedence
////    val heightData = data.demande
////
////    // variables
////    val starts: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
////    val duration: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
////    val ends: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => starts(i) + duration(i))
////
////    // constraint
////    for ((i, j) <- precedence)
////      add(ends(i) <= starts(j))
////
////    for (r <- 0 until nRes) {
////      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
////      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
////      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
////    }
////
////    val fakestart = CPIntVar(0 to horizon)
////    // bucketing the starts variable
////    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart)
////    val bucket: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(0 until nPart))
////    for (i <- starts.indices) {
////      val part = new Partitions(nPart, fakestart) //starts(i))
////      //      val part = new Partitions(nPart, starts(i))
////      bounds(i) = part.nContiguousBound()
////      add(BucketView(starts(i), bucket(i), bounds(i)))
////    }
////
////    // search
////    search {
////      conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
////    }
////
////    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
////    // on solution
////    onSolution {
////      val tuple = bucket.map(_.value)
////      table += tuple
////      //println(table.size + " " + tuple.mkString(" - "))
////    }
////    val stat = start(timeLimit = remainingTime.value)
////    println(stat)
////
////    (table.toArray,stat)
//  }
//
//}
