//package solve
//
//import bucket.PartitionEqualButLast
//import models.ModelWithTable
//import rcpsp.{Mut, RCPSPInstance}
//
////import bucket.{BucketView, Partitions}
////import oscar.cp._
////import oscar.cp.core.{CPPropagStrength, CPSolver}
////import oscar.cp.scheduling.constraints.MaxCumulative
////import rcpsp.{Mut, RCPSPInstance}
//
//object SolveUsingTable {
//
//  def apply(data: RCPSPInstance, remainingTime: Mut[Int], horizon: Int, nPart: Int, tableredundant:Array[Array[Int]]) = {
//    val model = new ModelWithTable(data,Array.tabulate(data.nTask)(i=> i),tableredundant,horizon,new PartitionEqualButLast(nPart))
//    model.loadModel
//    val stat = model.startBestSol(remainingTime.value)
//    println(stat)
//    if (stat.nSols == 0)
//      throw new Exception("There should be a solution")
//    val bestSoFarMakespan = model.getBestSoFar
//
//    (bestSoFarMakespan, stat)
//
////        implicit val solver = CPSolver()
////    val nTask = data.nTask
////    val nRes = data.nRes
////    val Capacity = data.Capacity
////    val durationData = data.duration
////    val precedence = data.precedence
////    val heightData = data.demande
////
////    // variables
////    val starts: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
////    val duration: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
////    val ends: Array[CPIntVar] =
////      Array.tabulate(nTask)(i => starts(i) + duration(i))
////
////    // constraint
////    for ((i, j) <- precedence)
////      add(ends(i) <= starts(j))
////
////    for (r <- 0 until nRes) {
////      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
////      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
////      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
////    }
////
////    val fakestart = CPIntVar(0 to horizon)
////    // bucketing the starts variable
////    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart)
////    val bucket: Array[CPIntVar] = Array.tabulate(nTask)(_ => CPIntVar(0 until nPart))
////    for (i <- starts.indices) {
////      val part = new Partitions(nPart, fakestart) //starts(i))
////      //      val part = new Partitions(nPart, starts(i))
////      bounds(i) = part.nContiguousBound()
////      add(BucketView(starts(i), bucket(i), bounds(i)))
////    }
////
////    add(shortTable(bucket, tableredundant,-1))
////
////    val makespan = maximum(ends)
////
////    minimize(makespan)(solver)
////
////    // search
////    search {
////      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
////    }
////
////    // on solution
////    onSolution {
////
////    }
////    val stat = start(timeLimit = remainingTime.value)
////    println(stat)
////
////    (0,stat)
//  }
//}
