package solve

import bucket.{BucketView, Partitions}
import oscar.algo.search.SearchStatistics
import oscar.cp.core.{CPPropagStrength, CPSolver}
import oscar.cp.scheduling.constraints.MaxCumulative
import oscar.cp._
import rcpsp.{Mut, RCPSPInstance}

import scala.collection.mutable.ArrayBuffer

object GenerateTableFSP {
  def apply(data: RCPSPInstance, horizon: Int, remainingTimeFSP: Mut[Int],  nPart: Mut[Int], nTaskToPart: Mut[Int]) : (Array[Array[Int]], SearchStatistics, Boolean) = {
    var finalTable = new ArrayBuffer[Array[Int]]()
    var shortFinalTable = new ArrayBuffer[Array[Int]]()
    var finalStat: SearchStatistics = null
    var isShortTable: Boolean = true
    var found = false
    val taskSortedByEnergy: Array[Int] = (0 until data.nTask).sortBy(i => -data.taskDuration(i) * (0 until data.nRes).map(r => data.taskUsage(i)(r)).sum)
    while (remainingTimeFSP.value > 0 && !found && nTaskToPart.value < data.nTask){
      val (table, stat) = OneGenerateTableFSP(data,horizon, remainingTimeFSP, nPart, nTaskToPart)
      if(stat.completed){
        finalTable.clear()
        finalTable ++= table
        finalStat = stat
        remainingTimeFSP.value -= (stat.time / 1000).toInt
        nTaskToPart.value += 1
      } else {
        if(finalTable.nonEmpty && table.nonEmpty){
          val incompletTable = extractTable(table, taskSortedByEnergy , nTaskToPart.value)
          val completTable = extractTable(finalTable.toArray, taskSortedByEnergy , nTaskToPart.value-1)
          var corresponding = false
          var i = 0
          while (!corresponding && i < completTable.length) {
            if (incompletTable.nonEmpty && isEqual(completTable(i), incompletTable.last)){
              corresponding = true
              //println(completTable(i).mkString(",") + "\n" + incompletTable.last.mkString(",") + "\n i := "+ i + "\n"+ completTable(i+1).mkString(","))
            }
            i += 1
          }
          //println("i : = " + i)
          while (i < completTable.length) {
            val tuple: Array[Int] = Array.tabulate(nTaskToPart.value)(k => if (k < nTaskToPart.value - 1) completTable(i)(k) else -1)
            incompletTable += tuple
            i += 1
          }
          shortFinalTable.clear()
          shortFinalTable ++= incompletTable
        }else{
          nTaskToPart.value -= 1
          isShortTable = false
        }
        found = true
      }
    }
    if(isShortTable)
      return (shortFinalTable.toArray, finalStat, isShortTable)
    else
      return (finalTable.toArray, finalStat, isShortTable)
  }

  def OneGenerateTableFSP(data: RCPSPInstance, horizon: Int, remainingTimeFSP: Mut[Int],  nPart: Mut[Int], nTaskToPart: Mut[Int]) = {
    implicit val solver = CPSolver()
    val nTask = data.nTask
    val nRes = data.nRes
    val Capacity = data.resCapacity
    val durationData = data.taskDuration
    val precedence = data.precedence
    val heightData = data.taskUsage

    // variables
    val starts: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
    val duration: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
    val ends: Array[CPIntVar] =
      Array.tabulate(nTask)(i => starts(i) + duration(i))

    // constraint
    for ((i, j) <- precedence)
      add(ends(i) <= starts(j))

    for (r <- 0 until nRes) {
      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
    }
    // sort task by total energy
    val taskSortedByEnergy: Array[Int] = (0 until nTask).sortBy(i => -durationData(i) * (0 until nRes).map(r => heightData(i)(r)).sum)

    val fakestart = CPIntVar(0 to horizon)
    // bucketing the starts variable
    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart.value)
    val bucket: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPart.value){
        val part = new Partitions(nPart.value, fakestart)
        bucket(i) = CPIntVar(0 until nPart.value)
        bounds(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucket(i), bounds(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucket(i) = CPIntVar(0)
        bounds(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucket(i), bounds(i)))
      }
    }

    // search
    search {
      conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
    }

    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    // on solution
    onSolution {
      val tuple = bucket.map(_.value)
      table += tuple
      //println(table.size + " " + tuple.mkString(" - "))
    }
    val stat = start(timeLimit = remainingTimeFSP.value)
    //println(stat)

    (table.toArray,stat)
  }

  def isEqual(a: Array[Int], b: Array[Int]): Boolean = {
    for (i <- a.indices if a(i) != b(i))
      return false
    true
  }

  def extractTable(incompletTable: Array[Array[Int]], tasksOrdered: Array[Int], lenght: Int): ArrayBuffer[Array[Int]] = {
    val extractTable: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    for (a <- incompletTable.indices) {
      val tuple: Array[Int] = new Array[Int](lenght)
      for (i <- 0 until lenght) {
        tuple(i) = incompletTable(a)(tasksOrdered(i))
      }
      extractTable += tuple
    }
    extractTable
  }

}
