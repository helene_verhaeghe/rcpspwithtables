package solve

import bucket.{BucketView, Partitions}
import oscar.algo.search.SearchStatistics
import oscar.cp.core.{CPPropagStrength, CPSolver}
import oscar.cp.scheduling.constraints.MaxCumulative
import oscar.cp._
import rcpsp.{Mut, RCPSPInstance}

import scala.collection.mutable.ArrayBuffer

object GenerateTableFVP {
  def apply(data: RCPSPInstance, horizon: Int, remainingTimeFVP: Mut[Int],  nPart: Mut[Int], nTaskToPart: Mut[Int]) : (Array[Array[Int]], SearchStatistics) = {
    var finalTable = new ArrayBuffer[Array[Int]]()
    var finalStat: SearchStatistics = null
    var found = false
    while (remainingTimeFVP.value > 0 && !found && nPart.value < horizon){
      val (table, stat) = OneGenerateTableFVP(data,horizon, remainingTimeFVP, nPart, nTaskToPart)
      if(stat.completed){
        finalTable.clear()
        finalTable ++= table
        finalStat = stat
        remainingTimeFVP.value -= (stat.time / 1000).toInt
        nPart.value += 2
      }else{
        nPart.value -= 2
        found = true
      }
    }
    (finalTable.toArray, finalStat)
  }
  def OneGenerateTableFVP(data: RCPSPInstance, horizon: Int, remainingTimeFVP: Mut[Int],  nPart: Mut[Int], nTaskToPart: Mut[Int]) = {
    implicit val solver = CPSolver()
    val nTask = data.nTask
    val nRes = data.nRes
    val Capacity = data.resCapacity
    val durationData = data.taskDuration
    val precedence = data.precedence
    val heightData = data.taskUsage

    // variables
    val starts: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
    val duration: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
    val ends: Array[CPIntVar] =
      Array.tabulate(nTask)(i => starts(i) + duration(i))

    // constraint
    for ((i, j) <- precedence)
      add(ends(i) <= starts(j))

    for (r <- 0 until nRes) {
      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
    }
    // sort task by total energy
    val taskSortedByEnergy: Array[Int] = (0 until nTask).sortBy(i => -durationData(i) * (0 until nRes).map(r => heightData(i)(r)).sum)

    val fakestart = CPIntVar(0 to horizon)
    // bucketing the starts variable
    val bounds: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPart.value)
    val bucket: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPart.value){
        val part = new Partitions(nPart.value, fakestart)
        bucket(i) = CPIntVar(0 until nPart.value)
        bounds(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucket(i), bounds(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucket(i) = CPIntVar(0)
        bounds(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucket(i), bounds(i)))
      }
    }

    // search
    search {
      conflictOrderingSearch(bucket, i => bucket(i).size, i => bucket(i).min)
    }

    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    // on solution
    onSolution {
      val tuple = bucket.map(_.value)
      table += tuple
      //println(table.size + " " + tuple.mkString(" - "))
    }
    val stat = start(timeLimit = remainingTimeFVP.value)
    //println(stat)

    (table.toArray,stat)
  }
}
