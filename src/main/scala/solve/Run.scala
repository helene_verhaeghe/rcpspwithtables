package solve

import bucket.PartitionStrategy
import models._
import rcpsp.RCPSPParser

object Run extends App {


  if (args.length < 1) {
    println("usage: RCSPwithTable datafile [-TO timeout] [-p init]")

  } else {
    val file = args(0)
    var info = false
    var TO = Integer.MAX_VALUE
    var problemType = ""
    var partStrat: PartitionStrategy = null
    var nbItem = -1
    var TOTable = Integer.MAX_VALUE
    var maxTable = -1
    var feedback = false
    var format = "BL"

    def process(args: List[String]): Boolean = {
      try {
        args match {
          case Nil =>
            true
          case "-TO" :: x :: rest =>
            TO = x.toInt
            process(rest)
          case "-info" :: rest =>
            info = true
            process(rest)
          case "-format" :: x :: rest =>
            format = x
            process(rest)
          case "-p" :: x :: rest =>
            x match {
              case "init" =>
                problemType = x
                process(rest)
              case "withFullTable" =>
                problemType = x
                process(rest)
              case "withPartialFixedTable" =>
                problemType = x
                rest match {
                  case i :: r =>
                    nbItem = i.toInt
                    process(r)
                }
              case "withPartialIterativeTable" =>
                problemType = x
                rest match {
                  case i :: j :: fb :: r =>
                    TOTable = i.toInt
                    maxTable = j.toInt
                    feedback = fb.toBoolean
                    process(r)
                }
            }
          case "-partition" :: x :: y :: rest =>
            partStrat = PartitionStrategy(x, y.toInt)
            process(rest)
          case "-log=yes" :: rest =>
            Log.printlog = true
            process(rest)
          case "-log=no" :: rest =>
            Log.printlog = false
            process(rest)
          case _ =>
            throw new WrongArgsException("wrong arguments " + args.mkString(" "))
        }
      }
      catch {
        case _: Throwable =>
          throw new WrongArgsException("wrong arguments " + args.mkString(" "))
      }
    }


    process(args.toList.drop(1))

    val data = new RCPSPParser().parser(file, format)
    var remainingTime = TO

    if (info)
      data.outputinfo()

    problemType match {
      case "" =>
      case "init" =>
        try {
          println(args.mkString(" "))
          val t = System.currentTimeMillis()
          Task.initProblem_solveToOptimal(data, TO)
          println("Total time (with preproc) (ms): " + (System.currentTimeMillis() - t))
        } catch {
          case e => println(e.getMessage)
        }
      case "withFullTable" =>
        try {
          println(args.mkString(" "))
          assert(partStrat != null, "The chosen option requires a partition strategy [-partition (\"eqButLast\"|\"eqMod\") nPart]")
          //println("withFullTable")
          val t = System.currentTimeMillis()
          // Step 1: find one solution to reduce the horizon
          val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime)
          remainingTime -= (statFirst.time / 1000).toInt
          // Step 2: create table using the first sol horizon (help reduce the table size)
          val t2 = System.currentTimeMillis()
          val (table, statTab) = Task.generateTable_tableForAllTasks(data, remainingTime, horizon, partStrat)
          remainingTime -= (statTab.time / 1000).toInt
          // Step 3: Solve the problem with the table as redundant constraint
          val t3 = System.currentTimeMillis()
          val (bestHorizon, statSol) = Task.problemWithTable_solveToOptimal(data, remainingTime, table, horizon, partStrat)
          val tend = System.currentTimeMillis()
          println("Total time (s): " + ((statFirst.time + statTab.time + statSol.time) / 1000.0))
          println("Step 1 time (with preproc) (ms): " + (t2 - t))
          println("Step 2 time (with preproc) (ms): " + (t3 - t2))
          println("Step 3 time (with preproc) (ms): " + (tend - t3))
          println("Total time (with preproc) (ms): " + (tend - t))
        } catch {
          case e => println(e.getMessage)
        }
      case "withPartialFixedTable" =>
        try {
          println(args.mkString(" "))
          assert(partStrat != null, "The chosen option requires a partition strategy [-partition (\"eqButLast\"|\"eqMod\") nPart]")
          //println("withPartialFixedTable")
          val t = System.currentTimeMillis()
          // Step 1: find one solution to reduce the horizon
          val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime)
          remainingTime -= (statFirst.time / 1000).toInt
          // Step 2: create table using the first sol horizon (help reduce the table size)
          val t2 = System.currentTimeMillis()
          val energy = new Energy(data)
          val index = energy.getNTask(nbItem)
          val (table, statTab) = Task.generateTable_tableForSomeTasks(data, remainingTime, horizon, partStrat, index)
          remainingTime -= (statTab.time / 1000).toInt
          // Step 3: Solve the problem with the table as redundant constraint
          val t3 = System.currentTimeMillis()
          val (bestHorizon, statSol) = Task.problemWithTable_solveToOptimal(data, remainingTime, table, horizon, partStrat)
          val tend = System.currentTimeMillis()
          println("Total search time (s): " + ((statFirst.time + statTab.time + statSol.time) / 1000.0))
          println("Step 1 time (with preproc) (ms): " + (t2 - t))
          println("Step 2 time (with preproc) (ms): " + (t3 - t2))
          println("Step 3 time (with preproc) (ms): " + (tend - t3))
          println("Total time (with preproc) (ms): " + (tend - t))
        } catch {
          case e => println(e.getMessage)
        }
      case "withPartialIterativeTable" =>
        try {
          println(args.mkString(" "))
          assert(partStrat != null, "The chosen option requires a partition strategy [-partition (\"eqButLast\"|\"eqMod\") nPart]")
          //        println("withPartialIterativeTable")
          val t = System.currentTimeMillis()
          // Step 1: find one solution to reduce the horizon
          val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime)
          remainingTime -= (statFirst.time / 1000).toInt
          // Step 2: create table using the first sol horizon (help reduce the table size)
          val t2 = System.currentTimeMillis()
          var remainingTimeTable = TOTable
          val (table, statTab) = Task.generateTable_discrepencyByEnergy(data, remainingTimeTable, maxTable, horizon, partStrat, new Energy(data), feedback)
          remainingTime -= (statTab.time / 1000).toInt
          // Step 3: Solve the problem with the table as redundant constraint
          val t3 = System.currentTimeMillis()
          val (bestHorizon, statSol) = Task.problemWithTable_solveToOptimal(data, remainingTime, table, horizon, partStrat)
          val tend = System.currentTimeMillis()
          println("Total time (s): " + ((statFirst.time + statTab.time + statSol.time) / 1000.0))
          println("Step 1 time (with preproc) (ms): " + (t2 - t))
          println("Step 2 time (with preproc) (ms): " + (t3 - t2))
          println("Step 3 time (with preproc) (ms): " + (tend - t3))
          println("Total time (with preproc) (ms): " + (tend - t))
        } catch {
          case e => println(e.getMessage)
        }
      case "withPartialIterativeShortTable" =>
      //        assert(partStrat != null,"The chosen option requires a partition strategy [-partition (\"eqButLast\"|\"eqMod\") nPart]")
      //        // Step 1: find one solution to reduce the horizon
      //        val (horizon, statFirst) = Task.initProblem_findOneSolution(data, remainingTime)
      //        remainingTime -= (statFirst.time / 1000).toInt
      //        // Step 2: create table using the first sol horizon (help reduce the table size)
      //        var remainingTimeTable = TOTable
      //        val (table, statTab) = Task.generateTable_discrepencyByEnergy(data, remainingTimeTable, horizon, partStrat,new Energy(data),feedback)
      //        remainingTime -= (statFirst.time / 1000).toInt
      //        // Step 3: Solve the problem with the table as redundant constraint
      //        val (bestHorizon,statSol) = Task.problemWithTable_solveToOptimal(data, remainingTime,table, horizon, partStrat)
      //        println("Total time (s): " + ((statFirst.time + statTab.time + statSol.time) / 1000.0))
    }

  }


}


class WrongArgsException(message: String) extends Exception(message)


object Log {
  var printlog = true

  def apply(str: String) = {
    if (printlog)
      println(str)
  }

  def start(str: String) = Log(str + ": Start")

  def end(str: String) = Log(str + ": End")

  def solution(str: String, horizon: Int, sdeVars: Array[Int]) = {
    if (printlog) {
      println(str + " : Solution")
      println("Horizon :\t" + horizon)
      println("Starts :\t" + sdeVars.mkString(","))
    }
  }

  def bestsolution(str: String, horizon: Int, sdeVars: Array[Int]) = {
    if (printlog) {
      println(str + " : Best Solution")
      println("Horizon :\t" + horizon)
      println("Starts :\t" + sdeVars.mkString(","))
    }
  }

  def table(str: String, table: RedundantTable, nPart: Int) = {
    if (printlog) {
      println(str + " : Table")
      println("Arity :\t" + table.index.length)
      println("Size :\t" + table.table.length)
      println("Load factor :\t" + (1.0 * table.table.length / Math.pow(nPart, table.index.length)))
    }
  }

  def besttable(str: String, table: RedundantTable, nPart: Int) = {
    if (printlog) {
      println(str + " : Best Table")
      println("Arity :\t" + table.index.length)
      println("Size :\t" + table.table.length)
      println("Load factor :\t" + (1.0 * table.table.length / Math.pow(nPart, table.index.length)))
    }
  }
}


object tettteeee extends App {

  val str = "\t2\tShaowei Cai and Xindi Zhang\tPure MaxSAT and Its Applications to Combinatorial Optimization via Linear Local Search\t\nX \t3\tJaroslav Bendík and Ivana Cerna\tReplication-Guided Enumeration of Minimal Unsatisfiable Subsets\t\nX\t4\tMartin Cooper\tStrengthening neighbourhood substitution\t\nX\t11\tEdward Lam, Peter Stuckey, Sven Koenig and T. K. Satish Kumar\tExact Approaches to the Multi-Agent Collective Construction Problem\t\nX\t16\tQuentin Cohen-Solal\tTractable Fragments of Temporal Sequences of Topological Information\t\nX\t23\tBuser Say, Jo Devriendt, Jakob Nordström and Peter Stuckey\tTheoretical and Experimental Results for Planning with Learned Binarized Neural Network Transition Models\t\nX\t25\tAlexey Ignatiev, Martin Cooper, Mohamed Siala, Emmanuel Hebrard and Joao Marques-Silva\tTowards Formal Fairness in Machine Learning\t\n\t26\tLoic Rouquette and Christine Solnon\tabstractXOR: A global constraint dedicated to differential cryptanalysis\t\nX\t27\tRodothea Myrsini Tsoupidi, Roberto Castañeda Lozano and Benoit Baudry\tConstraint-Based Software Diversification for Efficient Mitigation of Code-Reuse Attacks\t\nX\t28\tMarko Kleine Büning, Carsten Sinz and Philipp Kern\tVerifying Equivalence Properties of Neural Networks with ReLU Activation Functions\t\nX\t29\tStephan Gocht, Ross McBride, Ciaran McCreesh, Jakob Nordström, Patrick Prosser and James Trimble\tCertifying Solvers for Clique and Maximum Common (Connected) Subgraph Problems\t\nX\t31\tRahul Gupta, Subhajit Roy and Kuldeep S. Meel\tPhase Transition Behaviour in Knowledge Compilation\t\nX\t32\tPaulius Dilkas and Vaishak Belle\tGenerating Random Logic Programs Using Constraint Programming\t\nX\t34\tTomáš Dlask and Tomáš Werner\tBounding Linear Programs by Constraint Propagation: Application to Max-SAT\t\nX\t36\tRoberto Amadini, Graeme Gange and Peter J. Stuckey\tDashed strings and the replace(-all) constraint\t\nX\t37\tBegum Genc and Barry O'Sullivan\tA Two-Phase Constraint Programming Model for Examination Timetabling at University College Cork\t\nX\t39\tNeng-Fa Zhou\tIn Pursuit of an Efficient SAT Encoding for the Hamiltonian Cycle Problem\t\nX\t41\tNicolas Isoart and Jean-Charles Régin\tParallelization of TSP solving in CP\t\nX\t51\tMonika Trimoska, Sorina Ionica and Gilles Dequen\tParity (XOR) Reasoning for the Index Calculus Attack\t\nX\t53\tMinghao Liu, Fan Zhang, Pei Huang, Shuzi Niu, Feifei Ma and Jian Zhang\tLearning the Satisfiability of Pseudo-Boolean Problem with Graph Neural Networks\t\nX\t55\tGustav Björdal, Pierre Flener, Justin Pearson, Peter J. Stuckey and Guido Tack\tSolving Satisfaction Problems using Large-Neighbourhood Search\t\nX\t60\tJohannes K. Fichte, Markus Hecher and Stefan Szeider\tA Time Leap Challenge for SAT-Solving\t\nX\t62\tTomáš Dlask and Tomáš Werner\tOn Relation Between Constraint Propagation and Block-Coordinate Descent in Linear Programs\t\nX\t63\tYannick Carissan, Chisom-Adaobi Dim, Denis Hagebaum-Reignier, Nicolas Prcovic, Cyril Terrioux and Adrien Varet\tComputing the Local Aromaticity of Benzenoids Thanks to Constraint Programming\t\nX\t64\tYannick Carissan, Denis Hagebaum-Reignier, Nicolas Prcovic, Cyril Terrioux and Adrien Varet\tUsing Constraint Programming to Generate Benzenoid Structures in Theoretical Chemistry\t\nX\t66\tEwan Davidson, Ozgur Akgun, Joan Espasa Arxer and Peter Nightingale\tEffective Encodings of Constraint Programming Models to SMT\t\nX\t71\tGordon Hoi, Sanjay Jain and Frank Stephan\tA Faster Exact Algorithm to Count X3SAT Solutions\t\nX\t73\tJinqiang Yu, Alexey Ignatiev, Peter Stuckey and Pierre Le Bodic\tComputing Optimal Decision Sets with SAT\t\nX\t74\tAlexander Ek, Maria Garcia de la Banda, Andreas Schutt, Peter J. Stuckey and Guido Tack\tAggregation and Garbage Collection for Online Optimization\t\n\t75\tDimosthenis C. Tsouros, Kostas Stergiou and Christian Bessiere\tOmissions in Constraint Acquisition\t\nX\t78\tJohannes K. Fichte, Norbert Manthey, Andre Schidler and Julian Stecklina\tTowards Faster Reasoners by using Transparent Huge Pages\t\nX\t79\tGentzel Rebecca, Laurent Michel and Van Hoeve Willem\tHADDOCK: A Language and Architecture for Decision Diagram Compilation\t\nX\t82\tCéline Brouard, Simon de Givry and Thomas Schiex\tPushing data into CP models using Graphical Model Learning and Solving\t\nX\t85\tJo Devriendt\tWatched Propagation of 0-1 Integer Linear Constraints\t\nX\t86\tBehrouz Babaki, Bilel Omrani and Gilles Pesant\tCombinatorial Search in CP-Based Iterated Belief Propagation\t\nX\t91\tJohannes K. Fichte, Markus Hecher and Stefan Szeider\tBreaking Symmetries with RootClique and LexTopsort\t\nX\t92\tAnastasia Paparrizou and Hugues Wattez\tEffective Perturbations for Constraint Solving\t\nX\t96\tJohannes K. Fichte, Markus Hecher and Maximilian Kieler\tTreewidth-Aware Quantifier Elimination and Expansion for QCSP\t\nX\t102\tJeffrey M. Dudek, Vu H. N. Phan and Moshe Y. Vardi\tDPMC: Weighted Model Counting by Dynamic Programming on Project-Join Trees\t\nX\t105\tMathieu Collet, Arnaud Gotlieb, Nadjib Lazaar, Mats Carlsson, Dusica Marijan and Morten Mossige\tRobTest: A CP Approach to Generate Maximal Test Trajectories for Industrial Robots\t\n\t108\tSimon Rohou, Abderahmane Bedouhene, Gilles Chabert, Alexandre Goldsztejn, Luc Jaulin, Bertrand Neveu, Victor Reyes and Gilles Trombettoni\tTowards a Generic Interval Solver for Differential-Algebraic CSP\t\n\t110\tIan Howell, Berthe Choueiry and Hongfeng Yu\tVisualizations to Summarize Search Behavior\t\nX\t113\tAlexandre Mercier-Aubin, Ludwig Dumetz, Jonathan Gaudreault and Claude-Guy Quimper\tThe Confidence Constraint: A Step Towards Stochastic CP Solvers\t\nX\t114\tSaeed Nejati, Ludovic Le Frioux and Vijay Ganesh\tA Machine Learning based Splitting Heuristic for Divide-and-Conquer Solvers\t\nX\t115\tKevin Leo, Graeme Gange, Maria Garcia De La Banda and Mark Wallace\tCore-Guided Model Reformulation\t\nX\t121\tGraeme Gange and Peter J. Stuckey\tThe argmax constraint\t\n\t126\tTomáš Peitl and Stefan Szeider\tFinding the Hardest Formulas for Resolution\t\n\t127\tValentin Antuori, Emmanuel Hebrard, Marie-José Huguet, Siham Essodaigui and Alain Nguyen\tLeveraging Reinforcement Learning, Constraint Programming and Local Search: A Case Study in Car Manufacturing\t\n\t129\tKyle E. C. Booth, Jeffrey Marshall, Bryan O'Gorman, Stuart Hadfield and Eleanor Rieffel\tQuantum-accelerated global constraint filtering\t\nX\t130\tJanne I. Kokkala and Jakob Nordstrom\tUsing Resolution Proofs to Analyse CDCL Solvers\t\nX\t132\tMargaux Nattaf and Arnaud Malapert\tFiltering rules for flow time minimization in a Parallel Machine Scheduling Problem\t\n\t133\tVaidyanathan P. R. and Stefan Szeider\tMaxSAT-Based Postprocessing for Treedepth\t\nX\t138\tEdward Lam, Frits de Nijs, Peter Stuckey, Donald Azuatalam and Ariel Liebman\tLarge Neighborhood Search for Temperature Control with Demand Response\t\nX\t139\tLucas Groleaz, Samba-Ndojh Ndiaye and Christine Solnon\tSolving the Group Cumulative Scheduling Problem with CPO and ACO\t\nX\t145\tRémy Garcia, Claude Michel and Michel Rueher\tA branch-and-bound algorithm to rigorously enclose the round-off errors"


  val strfilt = str.split("\n").map(_.split("\t")(2)).map(_.split(" and "))
    .map(x => if (x.length > 1) x(0) + ", " + x(1) else (x(0))).map(_.split(", "))

  println(strfilt.map(_.mkString("\n")).mkString("\n"))


}