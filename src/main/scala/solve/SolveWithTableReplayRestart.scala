package solve

import bucket.BucketView
import oscar.algo.search.{DFSLinearizer, SearchStatistics}
import oscar.cp.core.{CPPropagStrength, CPSolver}
import oscar.cp.scheduling.constraints.MaxCumulative
import oscar.cp._
import rcpsp.{Mut, RCPSPInstance}
import bucket.Partitions

object SolveWithTableReplayRestart {
  def apply(data: RCPSPInstance, horizon: Int, remainingTime: Int,
            remainingTimeSPVP: Mut[Int],  nPartSPVP: Mut[Int], nTaskToPartSPVP: Mut[Int],
            remainingTimeFVP: Mut[Int],  nPartFVP: Mut[Int], nTaskToPartFVP: Mut[Int],
            remainingTimeFSP: Mut[Int],  nPartFSP: Mut[Int], nTaskToPartFSP: Mut[Int]):
            (SearchStatistics, SearchStatistics, SearchStatistics, SearchStatistics,
            SearchStatistics, SearchStatistics, SearchStatistics) = {
    implicit val solver = CPSolver()
    solver.silent = true
    val nTask = data.nTask
    val nRes = data.nRes
    val Capacity = data.resCapacity
    val durationData = data.taskDuration
    val precedence = data.precedence
    val heightData = data.taskUsage

    // variables
    val starts: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i)))
    val duration: Array[CPIntVar] =
      Array.tabulate(nTask)(i => CPIntVar(durationData(i)))
    val ends: Array[CPIntVar] =
      Array.tabulate(nTask)(i => starts(i) + duration(i))
    // sort task by total energy
    val taskSortedByEnergy: Array[Int] = (0 until nTask).sortBy(i => -durationData(i) * (0 until nRes).map(r => heightData(i)(r)).sum)
    val fakestart = CPIntVar(0 to horizon)

    // build bucket variable for the replay of SPVP model
    val (tableSPVP, statTableSPVP) = GenerateTableSPVP(data, horizon, remainingTimeSPVP, nPartSPVP, nTaskToPartSPVP)
    // bucketing the starts variable
    val boundsSPVP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartSPVP.value)
    val bucketSPVP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartSPVP.value){
        val part = new Partitions(nPartSPVP.value, fakestart)
        bucketSPVP(i) = CPIntVar(0 until nPartSPVP.value)(solver)
        boundsSPVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketSPVP(i), boundsSPVP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketSPVP(i) = CPIntVar(0)(solver)
        boundsSPVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketSPVP(i), boundsSPVP(i)))
      }
    }

    // build bucket variable for the replay of FVP model
    val (tableFVP, statTableFVP) = GenerateTableFVP(data, horizon, remainingTimeFVP, nPartFVP, nTaskToPartFVP)
    // bucketing the starts variable
    val boundsFVP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartFVP.value)
    val bucketFVP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    //println(taskSortedByEnergy.mkString("--") + "\n" + taskSortedByEnergy.length + "\n"+ nTaskToPartFVP.value)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartFVP.value){
        val part = new Partitions(nPartFVP.value, fakestart)
        bucketFVP(i) = CPIntVar(0 until nPartFVP.value)(solver)
        boundsFVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFVP(i), boundsFVP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketFVP(i) = CPIntVar(0)(solver)
        boundsFVP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFVP(i), boundsFVP(i)))
      }
    }
    // build bucket variable for the replay of FSP model
    val (tableFSP, statTableFSP, isShortTable) = GenerateTableFSP(data, horizon, remainingTimeFSP, nPartFSP, nTaskToPartFSP)
    // bucketing the starts variable
    val boundsFSP: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](nTask, nPartFSP.value)
    val bucketFSP: Array[CPIntVar] = new Array[CPIntVar](nTask)
    for (i <- taskSortedByEnergy) {
      if(taskSortedByEnergy.indexOf(i) < nTaskToPartFSP.value){
        val part = new Partitions(nPartFSP.value, fakestart)
        bucketFSP(i) = CPIntVar(0 until nPartFSP.value)(solver)
        boundsFSP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFSP(i), boundsFSP(i)))
      }else{
        val part = new Partitions(1, fakestart)
        bucketFSP(i) = CPIntVar(0)(solver)
        boundsFSP(i) = part.nContiguousBound()
        add(BucketView(starts(i), bucketFSP(i), boundsFSP(i)))
      }
    }

    // constraint
    for ((i, j) <- precedence)
      add(ends(i) <= starts(j))

    for (r <- 0 until nRes) {
      val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
      val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
      add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
    }

    val makespan = maximum(ends)

    minimize(makespan)(solver)

    // search
    search {
      conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
    }

    // on solution
    onSolution {
      //println("start: " + starts.mkString(" - ")  +"\nmakeSpan: " + makespan)
    }
    val linearizer: DFSLinearizer = new DFSLinearizer()
    //println("basic model without table")
    val statBasic = startSubjectTo(searchListener = linearizer, timeLimit = remainingTime){

    }
    //println(statBasic)
    var statReplaySPVP: SearchStatistics = null
    var statReplayFVP: SearchStatistics = null
    var statReplayFSP: SearchStatistics = null
    var statRestartSPVP: SearchStatistics = null
    var statRestartFVP: SearchStatistics = null
    var statRestartFSP: SearchStatistics = null

    //if(statBasic.time > remainingTimeFSP.value*1000){
      /*
    Replay the basic model with different types of table
     */

      //println("replay with table SPVP")
      statReplaySPVP = solver.replaySubjectTo(linearizer, starts, timeLimit = remainingTime){
        //println(tableSPVP.length)
        solver.objective.objs.head.relax()
        if(tableSPVP.nonEmpty)
          add(table(bucketSPVP, tableSPVP))
      }
      //println(statReplaySPVP)
      //println("replay with table FVP")
      statReplayFVP = solver.replaySubjectTo(linearizer, starts, timeLimit = remainingTime){
        //println(tableFVP.length)
        solver.objective.objs.head.relax()
        if(tableFVP.nonEmpty)
          add(table(bucketFVP, tableFVP))
      }
      //println(statReplayFVP)
      //println("replay with table FSP")
      statReplayFSP = solver.replaySubjectTo(linearizer, starts, timeLimit = remainingTime){
        //println(tableFSP.length + "    " + tableFSP.head.length + "  "+ nTaskToPartFSP.value + "  " + isShortTable)
        //println(tableFSP.last.mkString(",") + " \n" + tableFSP.head.mkString(","))
        solver.objective.objs.head.relax()
        if(tableFSP.nonEmpty){
          if(isShortTable){
            val partialBucketFSP: Array[CPIntVar] = Array.tabulate(nTaskToPartFSP.value)(i => bucketFSP(taskSortedByEnergy(i)))
            add(shortTable(partialBucketFSP, tableFSP))
          }else{
            add(table(bucketFSP, tableFSP))
          }

        }
      }
      //println(statReplayFSP)
      /*
      Restart the basic model with different types of table
       */
      //println("restart with table SPVP")
      statRestartSPVP = startSubjectTo(timeLimit = remainingTime){
        //println(tableSPVP.length)
        solver.objective.objs.head.relax()
        if(tableSPVP.nonEmpty)
          add(table(bucketSPVP, tableSPVP))
      }
      //println(statRestartSPVP)
      //println("replay with table FVP")
      statRestartFVP = startSubjectTo(timeLimit = remainingTime){
        //println(tableFVP.length)
        solver.objective.objs.head.relax()
        if(tableFVP.nonEmpty)
          add(table(bucketFVP, tableFVP))
      }
      //println(statRestartFVP)
      //println("restart with table FSP")
      statRestartFSP = startSubjectTo(timeLimit = remainingTime){
        //println(tableFSP.length)
        solver.objective.objs.head.relax()
        if(tableFSP.nonEmpty){
          if(isShortTable){
            val partialBucketFSP: Array[CPIntVar] = Array.tabulate(nTaskToPartFSP.value)(i => bucketFSP(taskSortedByEnergy(i)))
            add(shortTable(partialBucketFSP, tableFSP))
          }else{
            add(table(bucketFSP, tableFSP))
          }
        }
      }
      //println(statRestartFSP)
    //}
    println(tableSPVP.length + " | " + tableFVP.length + " | " + tableFSP.length)

    (statBasic, statReplaySPVP, statReplayFVP, statReplayFSP, statRestartSPVP, statRestartFVP, statRestartFSP)
  }
}
