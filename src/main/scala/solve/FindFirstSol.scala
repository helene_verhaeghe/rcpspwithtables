//package solve
//
//import models.BasicModel
//import rcpsp._
//
//object FindFirstSol {
//
//  def apply(data: RCPSPInstance, remainingTime: Mut[Int]) = {
//    val model = new BasicModel(data)
//    model.loadModel
//    val stat = model.startOneSol(remainingTime.value)
//    println(stat)
//    if (stat.nSols == 0)
//      throw new Exception("There should be a first solution")
//    val bestSoFarMakespan = model.getBestSoFar
//
//    (bestSoFarMakespan, stat)
//
////        implicit val solver = CPSolver()
////        val nTask = data.nTask
////        val nRes = data.nRes
////        val precedence = data.precedence
////        val Capacity = data.Capacity
////        val heightData = data.demande
////        val durationData = data.duration
////        val horizon = durationData.sum
////
////        // variables
////        val starts: Array[CPIntVar] =
////          Array.tabulate(nTask)(i => CPIntVar(0 to horizon - durationData(i))(solver))
////        val duration: Array[CPIntVar] =
////          Array.tabulate(nTask)(i => CPIntVar(durationData(i))(solver))
////        val ends: Array[CPIntVar] =
////          Array.tabulate(nTask)(i => starts(i) + duration(i))
////
////        // add constraints
////        // precedence constraint
////        for ((s, e) <- precedence) {
////          add(ends(s) <= starts(e))(solver)
////        }
////
////        // resource constraint
////        for (r <- 0 until nRes) {
////          val resource: Array[CPIntVar] = Array.fill(nTask)(CPIntVar(r)(solver))
////          val height: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(heightData(i)(r))(solver))
////          add(MaxCumulative(starts, duration, ends, height, resource, CPIntVar(Capacity(r))(solver), r), CPPropagStrength.Strong)(solver)
////        }
////
////        // objective function
////        val makespan = maximum(ends)
////        minimize(makespan)(solver)
////        var bestSoFarMakespan = Integer.MAX_VALUE
////
////        // On solution
////        onSolution {
////          bestSoFarMakespan = makespan.value
////          println(starts.mkString(" , ") + "\nmakespan: " + makespan.value + "\n-----------------------------")
////        }(solver)
////
////        // search
////        search {
////          conflictOrderingSearch(starts, i => starts(i).size, i => starts(i).min)
////        }(solver)
////
////        val stat = start(timeLimit = remainingTime.value, nSols = 1)(solver)
////
////        println(stat)
////
////        if (stat.nSols == 0)
////          throw new Exception("There should be a first solution")
////
////        (bestSoFarMakespan, stat)
//  }
//}
//
