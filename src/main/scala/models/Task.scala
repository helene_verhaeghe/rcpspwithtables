package models

import bucket.PartitionStrategy
import oscar.algo.search.SearchStatistics
import oscar.cp.core.CPPropagStrength
import oscar.cp.{maximum, minimize, onSolution, start}
import rcpsp.RCPSPInstance
import solve.Log

import scala.collection.mutable.ArrayBuffer

object Task {

  /**
   * Initial problem (P) : problem without modifictation
   * Initial problem enhanced with a redundant table (P+T): problem + redundant constraint made from a table
   * Approximate problem (P') : approximation of P with bucket vars, meant to generate table
   */


  /**
   * Solve P to the optimality
   *
   * @param data
   * @param remainingTime time limit
   * @return
   */
  private def initProblem_solveToOptimalModulable(data: RCPSPInstance, remainingTime: Int, searchfct: ModelStartDurationEndVars => Unit, searchstr: String) = {
    val str = "Search for all solution of P (" + searchstr + ")"

    Log.start(str)
    val solver = Model.createSolver
    solver.silent = true
    val sdeVars = Model.createStartDurationEndVars(solver, data.nTask, data.taskDuration, data.maxHorizon)
    Model.addPrecedenceCst(sdeVars, data.precedence)
    Model.addCumulativeCst(sdeVars, data.taskUsage, data.nRes, data.resCapacity)

    var bestSoFarMakespan = Integer.MAX_VALUE
    val bestSoFarStarts = Array.fill(sdeVars.starts.length)(0)
    // objective function
    val makespan = maximum(sdeVars.ends)
    minimize(makespan)(solver)
    // On solution
    onSolution {
      if (bestSoFarMakespan > makespan.value) {
        bestSoFarMakespan = makespan.value
        for (i <- bestSoFarStarts.indices)
          bestSoFarStarts(i) = sdeVars.starts(i).value
        Log.solution(str, bestSoFarMakespan, bestSoFarStarts)
      }
    }(solver)

    searchfct(sdeVars)
    Model.addCOSStart(sdeVars)

    val stat = start(timeLimit = remainingTime)(solver)

    Log.end(str)
    Log.bestsolution(str, bestSoFarMakespan, bestSoFarStarts)
    Log(str + ": Stats")
    Log(stat.toString)

    (bestSoFarMakespan, stat)
  }

  def initProblem_solveToOptimal(data: RCPSPInstance, remainingTime: Int) = {
    initProblem_solveToOptimalModulable(data, remainingTime, sdeVars => Model.addCOSStart(sdeVars), "COS")
  }

  def initProblem_solveToOptimalSplit(data: RCPSPInstance, remainingTime: Int) = {
    initProblem_solveToOptimalModulable(data, remainingTime, sdeVars => Model.addSplitStart(sdeVars), "Split")
  }

  def initProblem_solveToOptimalFF(data: RCPSPInstance, remainingTime: Int) = {
    initProblem_solveToOptimalModulable(data, remainingTime, sdeVars => Model.addFFStart(sdeVars), "FF")
  }

  /**
   * Find one first sol of P to have an upperbound on the span
   *
   * @param data
   * @param remainingTime
   * @return
   */
  def initProblem_findOneSolution(data: RCPSPInstance, remainingTime: Int) = {
    val str = "Search for first solution of P (COS)"

    Log.start(str)
    val solver = Model.createSolver
    val sdeVars = Model.createStartDurationEndVars(solver, data.nTask, data.taskDuration, data.maxHorizon)
    Model.addPrecedenceCst(sdeVars, data.precedence)
    Model.addCumulativeCst(sdeVars, data.taskUsage, data.nRes, data.resCapacity, CPPropagStrength.Medium)

    var bestSoFarMakespan = Integer.MAX_VALUE
    val bestSoFarStarts = Array.fill(sdeVars.starts.length)(0)
    // objective function
    val makespan = maximum(sdeVars.ends)
    minimize(makespan)(solver)
    // On solution
    onSolution {
      if (bestSoFarMakespan > makespan.value) {
        bestSoFarMakespan = makespan.value
        for (i <- bestSoFarStarts.indices)
          bestSoFarStarts(i) = sdeVars.starts(i).value
        Log.solution(str, bestSoFarMakespan, bestSoFarStarts)
      }
    }(solver)

    Model.addCOSStart(sdeVars)

    val stat = start(timeLimit = remainingTime, nSols = 1)(solver)

    Log.end(str)
    Log.bestsolution(str, bestSoFarMakespan, bestSoFarStarts)
    Log(str + ": Stats")
    Log(stat.toString)

    (bestSoFarMakespan, stat)

  }


  /**
   * Solve P' and get a table from it
   *
   * @param data
   * @param remainingTime
   * @param horizon
   * @param partStrat
   * @return
   */
  def generateTable_tableForAllTasks(data: RCPSPInstance, remainingTime: Int, horizon: Int, partStrat: PartitionStrategy) = {

    val index = Array.tabulate(data.nTask)(i => i)
    generateTable_tableForSomeTasks(data, remainingTime, horizon, partStrat, index)
  }

  /**
   * Solve P' and get a table from it over only a subset of the tasks
   *
   * @param data
   * @param remainingTime
   * @param horizon
   * @param partStrat
   * @param index
   * @return
   */
  def generateTable_tableForSomeTasks(data: RCPSPInstance, remainingTime: Int, horizon: Int, partStrat: PartitionStrategy, index: Array[Int],withError:Boolean = true) = {

    val str = "Search for all solution of P' (COS)"

    Log.start(str)
    val solver = Model.createSolver
    val sdeVars = Model.createStartDurationEndVars(solver, data.nTask, data.taskDuration, horizon)
    Model.addPrecedenceCst(sdeVars, data.precedence)
    Model.addCumulativeCst(sdeVars, data.taskUsage, data.nRes, data.resCapacity)

    val bVars = Model.createBuckets(solver, sdeVars, partStrat, index, horizon)

    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    // On solution
    onSolution {
      val tuple = bVars.bucket.map(_.value)
      table += tuple
    }(solver)

    Model.addCOSBucket(bVars)

    val stat = start(timeLimit = remainingTime)(solver)

    if (withError && !stat.completed)
      throw new Exception("The table is not complete")

    Log.end(str)
    val tab = RedundantTable(index, table.toArray)
    Log.table(str, tab, partStrat.nPart)
    Log(str + ": Stats")
    Log(stat.toString)

    (RedundantTable(index, table.toArray), stat)
  }

  /**
   * Solve P' with a redundant table and get a table from it over only a subset of the tasks
   *
   * @param data
   * @param remainingTime
   * @param horizon
   * @param partStrat
   * @param index
   * @return
   */
  def generateTable_tableForSomeTasksWithTable(data: RCPSPInstance, remainingTime: Int, horizon: Int, partStrat: PartitionStrategy, index: Array[Int], tab: RedundantTable,withError:Boolean = true) = {

    val str = "Search for all solution of P'"

    Log.start(str)
    val solver = Model.createSolver
    val sdeVars = Model.createStartDurationEndVars(solver, data.nTask, data.taskDuration, horizon)
    Model.addPrecedenceCst(sdeVars, data.precedence)
    Model.addCumulativeCst(sdeVars, data.taskUsage, data.nRes, data.resCapacity)


    val bVars = Model.createBuckets(solver, sdeVars, partStrat, index, horizon)
    Model.addRedundantTableCstMinus(bVars, tab)

    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    // On solution
    onSolution {
      val tuple = bVars.bucket.map(_.value)
      table += tuple
    }(solver)

    Model.addCOSBucket(bVars)

    val stat = start(timeLimit = remainingTime)(solver)

    if (withError && !stat.completed)
      throw new Exception("The table is not complete")

    Log.end(str)
    val newTable = RedundantTable(index, table.toArray)
    Log.table(str, newTable, partStrat.nPart)
    Log(str + ": Stats")
    Log(stat.toString)

    (newTable, stat)
  }

  /**
   * Solve P' by discrepency step and get a table from it over only a subset of the tasks
   *
   * @param data
   * @param remainingTime
   * @param horizon
   * @param partStrat
   * @param index
   * @return
   */
  def generateTable_discrepencyByEnergy(data: RCPSPInstance, allowedTime: Int, maxTable: Int, horizon: Int, partStrat: PartitionStrategy, ts: TaskSorter, feedback: Boolean) = {

    val str = "Search for all solution of P'"

    var n = 1
    // first iter
    var index = ts.getNTask(n)
    var (table, stat) = generateTable_tableForSomeTasks(data, allowedTime, horizon, partStrat, index)
    var s = new SearchStatistics(0, 0, stat.time, true, 0, 0, 0)
    n = n + 1
    val mTable = Math.min(maxTable, data.nTask)

    while (s.time / 1000 < allowedTime && n <= mTable) {

      index = ts.getNTask(n)
      val res = if (feedback)
        generateTable_tableForSomeTasksWithTable(data, allowedTime - (s.time / 1000).toInt, horizon, partStrat, index, table,false)
      else
        generateTable_tableForSomeTasks(data, allowedTime - (s.time / 1000).toInt, horizon, partStrat, index,false)

      stat = res._2
      if (stat.completed) {
        table = res._1
        s = new SearchStatistics(0, 0, s.time + stat.time, true, 0, 0, 0)
      } else {
        s = new SearchStatistics(0, 0, s.time + stat.time, true, 0, 0, 0)
      }

      n = n + 1
    }
    Log.besttable(str, table, partStrat.nPart)
    (table, s)

  }


  /**
   * Solve P+T
   *
   * @param data
   * @param remainingTime
   * @param horizon
   * @param partStrat
   * @return
   */
  def problemWithTable_solveToOptimal(data: RCPSPInstance, remainingTime: Int, tab: RedundantTable, horizon: Int, partStrat: PartitionStrategy) = {
    val str = "Search for all solution of P+T (COS)"


    Log.start(str)
    val solver = Model.createSolver
    val sdeVars = Model.createStartDurationEndVars(solver, data.nTask, data.taskDuration, horizon)
    Model.addPrecedenceCst(sdeVars, data.precedence)
    Model.addCumulativeCst(sdeVars, data.taskUsage, data.nRes, data.resCapacity)

    val bVars = Model.createBuckets(solver, sdeVars, partStrat, tab.index, horizon)
    Model.addRedundantTableCst(bVars, tab)

    var bestSoFarMakespan = Integer.MAX_VALUE
    val bestSoFarStarts = Array.fill(sdeVars.starts.length)(0)
    // objective function
    val makespan = maximum(sdeVars.ends)
    minimize(makespan)(solver)
    // On solution
    onSolution {
      if (bestSoFarMakespan > makespan.value) {
        bestSoFarMakespan = makespan.value
        for (i <- bestSoFarStarts.indices)
          bestSoFarStarts(i) = sdeVars.starts(i).value
        Log.solution(str, bestSoFarMakespan, bestSoFarStarts)
      }
    }(solver)

    Model.addCOSStart(sdeVars)

    val stat = start(timeLimit = remainingTime)(solver)

    Log.end(str)
    Log.bestsolution(str, bestSoFarMakespan, bestSoFarStarts)
    Log(str + ": Stats")
    Log(stat.toString)

    (bestSoFarMakespan, stat)
  }

}


case class RedundantTable(index: Array[Int], var table: Array[Array[Int]])