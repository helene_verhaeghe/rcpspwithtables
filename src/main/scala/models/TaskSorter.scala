package models

import rcpsp.RCPSPInstance

/**
 * Help sort the tasks following a heuristic
 */
abstract class TaskSorter {

  def getNTask(n:Int) : Array[Int]


}

/**
 * Sort the task in decreasing order of Energy
 */
class Energy(data: RCPSPInstance) extends TaskSorter {
  val nTask = data.nTask
  val nRes = data.nRes
  val durationData = data.taskDuration
  val heightData = data.taskUsage

  val order = (0 until nTask).sortBy(i => -durationData(i) * (0 until nRes).map(r => heightData(i)(r)).sum).toArray

  def getNTask(n:Int) = {
    val ncap = Math.min(n,data.nTask)
    val array = new Array[Int](ncap)
    System.arraycopy(order,0,array,0,ncap)
    array
  }
}
