package models

import bucket.PartitionStrategy
import oscar.cp._
import oscar.cp.core.{CPPropagStrength, CPSolver}
import oscar.cp.scheduling.constraints.MaxCumulative


object Model {

  /*
   * Create CP solver
   */
  def createSolver = CPSolver()

  /*
   * Create the start, duration and ends vars:
   *  - dom(start_i) = {0,...,h - d_i}
   *  - dom(duration_i) = {d_i}
   *  - end_i = start_i + duration_i
   */
  def createStartDurationEndVars(solver: CPSolver, nTask: Int, taskDuration: Array[Int], horizon: Int) = {
    val starts: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(0 to horizon - taskDuration(i), "Start[" + i + "]")(solver))
    val durations: Array[CPIntVar] = Array.tabulate(nTask)(i => CPIntVar(taskDuration(i))(solver))
    val ends: Array[CPIntVar] = Array.tabulate(nTask)(i => starts(i) + durations(i))
    ModelStartDurationEndVars(solver, nTask, horizon, starts, durations, ends)
  }

  /*
   * Create bucket vars for the start variable specified in index, the bucket is created following the specified partition strategy
   * Create and post the corresponding channeling constraint too
   */
  def createBuckets(solver: CPSolver, sdeVars: ModelStartDurationEndVars, partStrat: PartitionStrategy, index: Array[Int], horizon: Int) = {
    val bucket = Array.fill(index.length)(partStrat.getBucket(solver))
    for (i <- index.indices) {
      val idx = index(i)
      val cst = partStrat.getBucketView(sdeVars.starts(idx), bucket(i), horizon)
      add(cst)(solver)
    }
    ModelBucketVars(solver, horizon, index, bucket)
  }

  /*
   * Create and post the precedence constraints of the model
   */
  def addPrecedenceCst(sdeVars: ModelStartDurationEndVars, precedence: Array[(Int, Int)]) = {
    for ((s, e) <- precedence) {
      add(sdeVars.ends(s) <= sdeVars.starts(e))(sdeVars.solver)
    }
  }

  /*
   * Create and post the cumulative constraints of the model
   */
  def addCumulativeCst(sdeVars: ModelStartDurationEndVars, taskUsage: Array[Array[Int]], nRes: Int, resCapacity: Array[Int], propagStrength: CPPropagStrength = CPPropagStrength.Strong) = {
    for (r <- 0 until nRes) {
      val resourceVar: Array[CPIntVar] = Array.fill(sdeVars.nTask)(CPIntVar(r)(sdeVars.solver))
      val taskUsageVar: Array[CPIntVar] = Array.tabulate(sdeVars.nTask)(i => CPIntVar(taskUsage(i)(r))(sdeVars.solver))
      add(MaxCumulative(sdeVars.starts, sdeVars.durations, sdeVars.ends, taskUsageVar, resourceVar, CPIntVar(resCapacity(r))(sdeVars.solver), r), propagStrength)(sdeVars.solver)
    }
  }

  /*
   * Create and post the redundant table constraint
   */
  def addRedundantTableCst(bVars: ModelBucketVars, tab: RedundantTable) = {
    add(shortTable(bVars.bucket, tab.table, -1))(bVars.solver)
  }

  /*
   * Create and post the redundant table constraint
   * This is the case where there is one more var than the table
   */
  def addRedundantTableCstMinus(bVars: ModelBucketVars, tab: RedundantTable) = {
    add(shortTable(bVars.bucket.dropRight(1), tab.table, -1))(bVars.solver)
  }

  /*
   * Add a COS search on start variable
   */
  def addCOSStart(sdeVars: ModelStartDurationEndVars) = {
    search {
      conflictOrderingSearch(sdeVars.starts, i => sdeVars.starts(i).size, i => sdeVars.starts(i).min)
    }(sdeVars.solver)
  }

  def addSplitStart(sdeVars: ModelStartDurationEndVars) = {
    search {
      sdeVars.starts.sortBy(-_.size).find(!_.isBound) match {
        case None => noAlternative
        case Some(x) =>
          val mid = x.min + (x.max - x.min + 1) / 2
          branch {
            add {
              x < mid
            }(sdeVars.solver)
          } {
            add {
              x >= mid
            }(sdeVars.solver)
          }
      }
    }(sdeVars.solver)
  }

  /*
   * Add a First Fail search on start variable
   */
  def addFFStart(sdeVars: ModelStartDurationEndVars) = {
    search {
      binaryFirstFail(sdeVars.starts, x => x.min)
    }(sdeVars.solver)
  }

  /*
   * Add a COS search on bucket variable
   */
  def addCOSBucket(bVars: ModelBucketVars) = {
    search {
      conflictOrderingSearch(bVars.bucket, i => bVars.bucket(i).size, i => bVars.bucket(i).min)
    }(bVars.solver)
  }


}

/*
 * Regroup the main decision variables (starts, duration, ends)
 */
case class ModelStartDurationEndVars(solver: CPSolver, nTask: Int, horizon: Int, starts: Array[CPIntVar], durations: Array[CPIntVar], ends: Array[CPIntVar]) {

  val allvar = starts ++ ends
}

/*
 * Regroup the bucket decision variables
 */
case class ModelBucketVars(solver: CPSolver, horizon: Int, index: Array[Int], bucket: Array[CPIntVar])