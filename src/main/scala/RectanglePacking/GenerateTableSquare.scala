package RectanglePacking

import bucket.BucketView
import oscar.algo.branchings.MaxSelectionBranching
import oscar.cp.core.CPPropagStrength
import oscar.cp.scheduling.constraints.MaxCumulative
import oscar.cp._
import bucket.Partitions

import scala.collection.mutable.ArrayBuffer

object GenerateTableSquare {
  def apply(n: Int, nPart_xy: Int, nPart_lw: Int, typeOfTable: Int): Array[Array[Int]] = {
    implicit val solver = CPSolver()
    solver.silent = true

    val size: Array[Int] = Array.tabulate(n)(i => i+1)
    val energy: Array[Int] = Array.tabulate(n)(i => size(i) * size(i))

    val sizeSum = 2*size.sum
    val sizeMax = 2*size.max

    val total_length: CPIntVar = CPIntVar(0 to sizeSum)
    val total_width: CPIntVar = CPIntVar(0 to sizeSum)



    val x: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to sizeSum))
    val y: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to sizeSum))

    val Size: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(size))

    val fake_x = CPIntVar(0 to sizeSum)
    // bucketing on x variable
    val bounds_x: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](n, nPart_xy)
    val bucket_x: Array[CPIntVar] = new Array[CPIntVar](n)
    for (i <- x.indices) {
      if(i > n/2){
        val part = new Partitions(nPart_xy, fake_x)
        bounds_x(i) = part.nContiguousBound()
        bucket_x(i) = CPIntVar(0 until nPart_xy)
        add(BucketView(x(i), bucket_x(i), bounds_x(i)))
      }else{
        val part = new Partitions(1, fake_x)
        bounds_x(i) = part.nContiguousBound()
        bucket_x(i) = CPIntVar(0)
        add(BucketView(x(i), bucket_x(i), bounds_x(i)))
      }
    }

    val fake_y = CPIntVar(0 to sizeSum)
    // bucketing on y variable
    val bounds_y: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](n, nPart_xy)
    val bucket_y: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 until nPart_xy))
    for (i <- y.indices) {
      val part = new Partitions(nPart_xy, fake_y)
      bounds_y(i) = part.nContiguousBound()
      add(BucketView(y(i), bucket_y(i), bounds_y(i)))
    }

    val fake_length = CPIntVar(0 to sizeSum)
    // bucketing on length variable
    var bounds_length: Array[(Int, Int)] = new Array[(Int, Int)](nPart_lw)
    var bucket_length: CPIntVar = CPIntVar(0 until nPart_lw)
    val part_l = new Partitions(nPart_lw, fake_length)
    bounds_length = part_l.nContiguousBound()
    add(BucketView(fake_length, bucket_length, bounds_length))

    val fake_width = CPIntVar(0 to sizeSum)
    // bucketing on width variable
    var bounds_width: Array[(Int, Int)] = new Array[(Int, Int)](nPart_lw)
    var bucket_width: CPIntVar = CPIntVar(0 until nPart_lw)
    val part_w = new Partitions(nPart_lw, fake_width)
    bounds_width = part_w.nContiguousBound()
    add(BucketView(fake_width, bucket_width, bounds_width))

    solver.add(total_length >= total_width)

    for(i <- 0 until n){
      solver.add(x(i) + Size(i) <= total_length)
      solver.add(y(i) + Size(i) <= total_width)
    }

    val xx: Array[CPIntVar] = Array.tabulate(n)(i => x(i) + Size(i))
    val yy: Array[CPIntVar] = Array.tabulate(n)(i => y(i) + Size(i))

    val resource0: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0))
    val resource1: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(1))

    solver.add(MaxCumulative(x, Size, xx, Size, resource0, total_width, 0), CPPropagStrength.Strong)
    solver.add(MaxCumulative(y, Size, yy, Size, resource1, total_length, 1), CPPropagStrength.Strong)

    for (i <- 0 until n; j <- i + 1 until n) {
      add((xx(i) ?<= x(j)) || (xx(j) ?<= x(i)) || (yy(i) ?<= y(j)) || (yy(j) ?<= y(i)))
    }

    for(i <- 0 until n)
      solver.add(Size(i) * Size(i) === energy(i))

    solver.add(total_length === maximum(xx))
    solver.add(total_width === maximum(yy))

    typeOfTable match{
      case 0 => {
        val decisionVars = (for(i <- 0 until n; v <- 0 to nPart_xy if bucket_x(i).hasValue(v)) yield {
          val selectionVar = bucket_x(i).isEq(v)
          val optionalVars = bucket_y ++ Array(bucket_length, bucket_width)
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case 1 => {
        val decisionVars = (for(i <- 0 until n; v <- 0 to nPart_xy if bucket_x(i).hasValue(v)) yield {
          val selectionVar = bucket_x(i).isEq(v)
          val optionalVars = bucket_y
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case 2 => {
        val decisionVars = (for (v <- 0 to sizeSum if total_length.hasValue(v)) yield {
          val selectionVar = total_length.isEq(v)
          val optionalVars = Array(total_width) ++ bucket_x //++ bucket_y
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case 3 => {
        val decisionVars = (for (v <- 0 to sizeSum if total_width.hasValue(v)) yield {
          val selectionVar = total_width.isEq(v)
          val optionalVars = Array(total_length) ++ bucket_y //++ bucket_x
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case _ => System.out.println("Wrong case")
    }



    val table: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
    // on solution
    onSolution {
      typeOfTable match{
        case 0 => {
          val tuple: Array[Int] = new Array[Int](2*n+2)
          for(i <- 0 until n)
            tuple(i) = bucket_x(i).value
          for(i <- 0 until n)
            tuple(i+n) = bucket_y(i).value
          tuple(2*n) = bucket_length.value
          tuple(2*n+1) = bucket_width.value
          table += tuple
          println(table.size + "   ->   " + tuple.mkString(" - "))
        }
        case 1 => {
          val tuple: Array[Int] = new Array[Int](2*n)
          for(i <- 0 until n)
            tuple(i) = bucket_x(i).value
          for(i <- 0 until n)
            tuple(i+n) = bucket_y(i).value
          //for(i <- 0 until n)
          //tuple(i+2*n) = rotated(i).value
          table += tuple
          println(table.size + "   ->   " + tuple.mkString(" - "))
        }
        case 2 => {
          val tuple: Array[Int] = new Array[Int](n+2)
          tuple(0) = total_length.value
          tuple(1) = total_width.value
          for(i <- 0 until n)
            tuple(i+2) = bucket_x(i).value
          /*for(i <- 0 until n)
            tuple(i+2+n) = bucket_y(i).value*/
          table += tuple
          println(table.size + "   ->   " + tuple.mkString(" - "))
        }
        case 3 => {
          val tuple: Array[Int] = new Array[Int](n+2)
          tuple(0) = total_width.value
          tuple(1) = total_length.value
          for(i <- 0 until n)
            tuple(i+2) = bucket_y(i).value
          /*for(i <- 0 until n)
            tuple(i+2+n) = bucket_x(i).value*/
          table += tuple
          println(table.size + "   ->   " + tuple.mkString(" - "))
        }
        case _ => System.out.println("wrong case")
      }
    }

    val stat = start()
    println(stat)
    table.toArray
  }
}
