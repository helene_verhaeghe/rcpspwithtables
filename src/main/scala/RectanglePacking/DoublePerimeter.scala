package RectanglePacking

import bucket.BucketView
import oscar.algo.branchings.MaxSelectionBranching
import oscar.algo.search.SearchStatistics
import oscar.cp.core.CPPropagStrength
import oscar.cp.scheduling.constraints.MaxCumulative
import oscar.cp.{table, _}
import bucket.Partitions

object DoublePerimeter {
  def apply(n: Int, nPart_xy: Int, nPart_lw: Int, searchStrategy: Int, tableGen: Array[Array[Int]]): SearchStatistics = {
    implicit val solver = CPSolver()
    solver.silent = true
    // n is the number of rectangles
    val length: Array[Int] = Array.tabulate(n)(i => i+1)
    val width: Array[Int] = Array.tabulate(n)(i => 2*n-i-1)
    val energy: Array[Int] = Array.tabulate(n)(i => length(i) * width(i))

    val maxd = (length ++ width).max
    val maxx = (length ++ width).sum
    val maxy = (length ++ width).sum

    val total_length: CPIntVar = CPIntVar(0 to maxx)
    val total_width: CPIntVar = CPIntVar(0 to maxy)

    val x: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to maxx))
    val y: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to maxy))

    val rotated: Array[CPBoolVar] = Array.tabulate(n)(_ => CPBoolVar())
    val effective_width: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to maxd))
    val effective_length: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 to maxd))

    val fake_x = CPIntVar(0 to maxx)
    // bucketing on x variable
    val bounds_x: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](n, nPart_xy)
    val bucket_x: Array[CPIntVar] = new Array[CPIntVar](n)
    for (i <- x.indices) {
      if(i > n/2){
        val part = new Partitions(nPart_xy, fake_x)
        bounds_x(i) = part.nContiguousBound()
        bucket_x(i) = CPIntVar(0 until nPart_xy)
        add(BucketView(x(i), bucket_x(i), bounds_x(i)))
      }else{
        val part = new Partitions(1, fake_x)
        bounds_x(i) = part.nContiguousBound()
        bucket_x(i) = CPIntVar(0)
        add(BucketView(x(i), bucket_x(i), bounds_x(i)))
      }
    }

    val fake_y = CPIntVar(0 to maxy)
    // bucketing on y variable
    val bounds_y: Array[Array[(Int, Int)]] = Array.ofDim[(Int, Int)](n, nPart_xy)
    val bucket_y: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0 until nPart_xy))
    for (i <- y.indices) {
      val part = new Partitions(nPart_xy, fake_y)
      bounds_y(i) = part.nContiguousBound()
      add(BucketView(y(i), bucket_y(i), bounds_y(i)))
    }

    val fake_length = CPIntVar(0 to maxx)
    // bucketing on y variable
    var bounds_length: Array[(Int, Int)] = new Array[(Int, Int)](nPart_lw)
    var bucket_length: CPIntVar = CPIntVar(0 until nPart_lw)
    val part_l = new Partitions(nPart_lw, fake_length)
    bounds_length = part_l.nContiguousBound()
    add(BucketView(fake_length, bucket_length, bounds_length))

    val fake_width = CPIntVar(0 to maxy)
    // bucketing on y variable
    var bounds_width: Array[(Int, Int)] = new Array[(Int, Int)](nPart_lw)
    var bucket_width: CPIntVar = CPIntVar(0 until nPart_lw)
    val part_w = new Partitions(nPart_lw, fake_width)
    bounds_width = part_w.nContiguousBound()
    add(BucketView(fake_width, bucket_width, bounds_width))

    var bucket: Array[CPIntVar] = null
    searchStrategy match{
      case 0 => {
        bucket = bucket_x ++ bucket_y ++ Array(bucket_length, bucket_width)
      }
      case 1 => {
        bucket = bucket_x ++ bucket_y
      }
      case 2 => {
        bucket = Array(total_length, total_width) ++ bucket_x //++ bucket_y
      }
      case 3 => {
        bucket = Array(total_width, total_length) ++ bucket_y //++ bucket_x
      }
      case _ => System.out.println("Wrong case")
    }
    solver.add(total_length >= total_width)

    for(i <- 0 until n){
      solver.add((rotated(i) ?=== 1) ==> (effective_width(i) ?=== length(i)))
      solver.add((rotated(i) ?=== 0) ==> (effective_width(i) ?=== width(i)))

      solver.add((rotated(i) ?=== 1) ==> (effective_length(i) ?=== width(i)))
      solver.add((rotated(i) ?=== 0) ==> (effective_length(i) ?=== length(i)))
    }

    for(i <- 0 until n){
      solver.add(x(i) + effective_length(i) <= total_length)
      solver.add(y(i) + effective_width(i) <= total_width)
    }

    val xx: Array[CPIntVar] = Array.tabulate(n)(i => x(i) + effective_length(i))
    val yy: Array[CPIntVar] = Array.tabulate(n)(i => y(i) + effective_width(i))

    val resource0: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(0))
    val resource1: Array[CPIntVar] = Array.tabulate(n)(_ => CPIntVar(1))

    solver.add(MaxCumulative(x, effective_length, xx, effective_width, resource0, total_width, 0), CPPropagStrength.Strong)
    solver.add(MaxCumulative(y, effective_width, yy, effective_length, resource1, total_length, 1), CPPropagStrength.Strong)

    for (i <- 0 until n; j <- i + 1 until n) {
      add((xx(i) ?<= x(j)) || (xx(j) ?<= x(i)) || (yy(i) ?<= y(j)) || (yy(j) ?<= y(i)))
    }

    for(i <- 0 until n)
      solver.add(effective_length(i) * effective_width(i) === energy(i))

    solver.add(total_length === maximum(xx))
    solver.add(total_width === maximum(yy))
    val objective: CPIntVar = total_length * total_width

    solver.minimize(objective)

    onSolution{
      println("Objective : " + objective.value + "\n Size: "+energy.mkString(" - ")+ "\n Length: "+length.mkString(" - ") + "\n Width: "+width.mkString(" - "))
      /*println("Objective : " + total_length * total_width + "\n x: " + x.mkString(",")+ "\n xx: " + xx.mkString(",") +
        "\n effective_length : " + effective_length.mkString(",") + "\n y: "+y.mkString(",") + "\n yy: " + yy.mkString(",") +
        "\n effective_width : " + effective_width.mkString(",") + "\n rotated : "+ rotated.mkString(","))*/
    }

    searchStrategy match {
      case 0 => {
        val decisionVars = ( for(i <- 0 until n; v <- 0 to maxx if x(i).hasValue(v)) yield {
          val selectionVar = x(i)isEq(v)
          val optionalVars = y ++ Array(total_length, total_width)
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case 1 => {
        val decisionVars = (for (i <- 0 until n; v <- 0 to maxx if x(i).hasValue(v)) yield {
          val selectionVar = x(i).isEq(v)
          val optionalVars = y ++ rotated
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case 2 => {
        val decisionVars = (for (v <- 0 to maxy if total_length.hasValue(v)) yield {
          val selectionVar = total_length.isEq(v)
          val optionalVars = x ++ y ++ rotated
          CPOptionalSelection(selectionVar, optionalVars, binaryFirstFail(optionalVars, _.min))
        }).toArray
        val decVar = decisionVars.map(_.selectionVar).asInstanceOf[Array[CPIntVar]]
        search(MaxSelectionBranching(
          decisionVars,
          conflictOrderingSearch(decVar, i => decVar(i).size, i => decVar(i).max)
        ))
      }
      case _ => println("Search not specified")
    }
    val statBasic = start()

    val statRestartStrong = startSubjectTo(timeLimit = Int.MaxValue){
      println(tableGen.length)
      solver.objective.objs.head.relax()
      if(tableGen.nonEmpty)
        solver.add(table(bucket, tableGen))
    }

    println(statBasic)
    println(statRestartStrong)
    statBasic
  }

}
